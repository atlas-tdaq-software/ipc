////////////////////////////////////////////////////////////////////////
//    ipc_ers.cc
//
//    Sergei Kolos,    January 2009
//
//    description:
//      Change ERS debug and verbosity levels for given applications 
//		in a specific TDAQ partition
//
////////////////////////////////////////////////////////////////////////

#include <iostream>

#include <boost/regex.hpp>

#include <ers/ers.h>

#include <cmdl/cmdargs.h>
#include <owl/regexp.h>

#include <ipc/partition.h>
#include <ipc/core.h>

namespace
{
    struct Parameters
    {
	int affected_applications;
	int debug_level;
	int verbosity_level;
    };
}

void
doAction(	ipc::servant_ptr servant,
		const std::string & type_name,
		const std::string & name,
		Parameters & params,
		bool trace )
{    
    if ( CORBA::is_nil(servant) )
    {
        std::cout << "error" << std::endl;
	std::cerr << "ERROR [ipc_ers]: Can't change parameters for the \"" 
        	  << type_name << "\" object \"" << name << "\"." << std::endl;
	std::cerr << "Reason: the object does not exist or does not implement the ipc::servant interface" << std::endl;
        return ;
    }
    
    try
    {    
        if ( params.debug_level != INT_MIN )
        {
	    if ( trace )
	    {
		std::cout << ">> changing debug level for the \"" << name << "\" server ... ";
	    }
	    servant -> debug_level( params.debug_level );
	    if ( trace )
	    {
		std::cout << "ok" << std::endl;
	    }
	    params.affected_applications++;
        }
    }
    catch(CORBA::SystemException & e)
    {
        std::cout << "error" << std::endl;
        std::cerr << "ERROR [ipc_ers]: Can't change debug level for the \""
        	  << type_name << "\" object \"" << name << "\"." << std::endl;
        std::cerr << "Reason: the object does not exist " << std::endl;
    }
    
    try
    {    
        if ( params.verbosity_level != INT_MIN )
        {
	    if ( trace )
	    {
		std::cout << ">> changing verbosity level for the \"" << name << "\" server ... ";
	    }
	    servant -> verbosity_level( params.verbosity_level );
	    if ( trace )
	    {
		std::cout << "ok" << std::endl;
	    }
	    params.affected_applications++;
        }
    }
    catch(CORBA::SystemException & e)
    {
        std::cout << "error" << std::endl;
        std::cerr << "ERROR [ipc_ers]: Can't change verbosity level for the \""
        	  << type_name << "\" object \"" << name << "\"." << std::endl;
        std::cerr << "Reason: the object does not exist " << std::endl;
    }
}

void
browseServers(	const IPCPartition partition, 
		const std::string & type_name, 
		const boost::regex & object_criteria,
		Parameters & params,
		bool trace )
{    
    std::map< std::string, ipc::servant_var > objects;
    
    partition.getObjects<ipc::servant,ipc::use_cache,ipc::unchecked_narrow>( objects, type_name );
    
    std::map< std::string, ipc::servant_var >::iterator it = objects.begin();
    
    while( it != objects.end() )
    {
        if ( boost::regex_match( (*it).first, object_criteria ) )
        {
            doAction( (*it).second, type_name, (*it).first, params, trace );
        }
	it++;
    }
}

void browseContexts(	const IPCPartition partition, 
			const boost::regex & object_criteria, 
			const boost::regex & type_criteria,
                        Parameters & params,
			bool trace )
{            
    std::list< std::string > t_names;
    
    partition.getTypes( t_names );
    
    for ( std::list< std::string >::iterator it = t_names.begin(); it != t_names.end(); it++ )
    {    
	if ( trace )
	{
	    std::cout << ">> processing servers of the \"" << *it << "\" type ... " << std::endl;
	}
	
        if ( boost::regex_match( *it, type_criteria ) )
        {
            browseServers( partition, *it, object_criteria, params, trace );
        }
	
	if ( trace )
	{
	    std::cout << ">> done" << std::endl;
	}
    }
}

void browsePartition(	const IPCPartition partition,
			const boost::regex & object_criteria,
			const boost::regex & type_criteria,
                        Parameters & params,
			bool trace )
{

    if ( trace )
    {
	std::cout << ">> processing \"" << partition.name() << "\" partition ..." << std::endl;
    }
    
    browseContexts( partition, object_criteria, type_criteria, params, trace );
}

void browseRootPartition(	const boost::regex & object_criteria, 
				const boost::regex & type_criteria,
                                Parameters & params,
				bool trace )
{
    
    IPCPartition partition;
    
    std::list < IPCPartition> pl;

    IPCPartition::getPartitions( pl );

    for ( std::list < IPCPartition>::iterator it = pl.begin(); it != pl.end(); it++ )
    {
	try {
    	    browsePartition( *it, object_criteria, type_criteria, params, trace );
	}
	catch( daq::ipc::InvalidPartition & ex ) {
	    ers::error( ex );
	}
    }
    
    try {
        browsePartition( partition, object_criteria, type_criteria, params, trace );
    }
    catch( daq::ipc::InvalidPartition & ex ) {
	ers::error( ex );
    }
}

int main(int argc, char ** argv)
{
      // Declare arguments
    CmdArgBool trace		('t', "trace", "Trace execution.");
    CmdArgInt  verbosity	('v', "verbosity", "verbosity-level", "New verbosity level for remote applications.");
    CmdArgInt  debug		('d', "debug", "debug-level", "New debug level for remote applications.");
    CmdArgStr  server_name	('n', "name", "server-name",
					"A name of IPC object.\n"
					"Regular expresssions allowed.\n"
					"[4mNOTE[0m: [1m\".*\"[0m matches any name\n"
                                        "if not given then command will be applied to the partition server" );
    CmdArgStr  partition_name	('p', "partition", "partition-name", "Change verbosity and/or debug levels for this partition.");
    CmdArgBool ref		('R', "reference", "Print the IPC initial reference.");
    CmdArgStr  type_name	('i', "interface", "interface-name", 
					"A name of IPC interface.\n"
					"Regular expressions allowed.\n"
					"Valid interface names for example are: \n"
					"   mrs/router, is/repository, monitoring/sampler, rdb/cursor\n"
					"   rc/controller, dsa/supervisor, pmg/AGENT, etc.\n"
					"[4mNOTE[0m: [1m\".*\"[0m matches any name\n"
					"if not given then command will be applied to the partition server");
    
    try {
        IPCCore::init( argc, argv );
    }
    catch( daq::ipc::Exception & ex ) {
	ers::fatal( ex );
	return 1;
    }
    
       // Declare command object and its argument-iterator
    CmdLine  cmd(*argv, &verbosity, &debug, &trace, &partition_name, &type_name, &server_name, &ref, NULL);
    CmdArgvIter  arg_iter(--argc, ++argv);

    cmd.description( "Change ERS debug and verbosity levels for given applications in a specific TDAQ partition.\n" );
  
    server_name = ".*";
    type_name = ".*";
    
    // Parse arguments
    cmd.parse(arg_iter);
   
    //
    // Print location of the IPC reference file
    //
    if (    !( verbosity.flags() && CmdArg::GIVEN ) 
    	&&  !( debug.flags() && CmdArg::GIVEN ) )
    {
	std::cerr << "ERROR [ipc_ers]: Either verbosity or debug level parameter must be given." << std::endl;
        return 1;
    }
    
    if ( ref.flags() && CmdArg::GIVEN )
    {
	std::cout << "Initial reference is " 
        	<< ipc::util::getInitialReference() << std::endl;
    }
    
    boost::regex object_criteria;
    try {
	object_criteria = boost::regex( server_name );
    }
    catch ( boost::bad_expression & ex ){
	std::cerr << "ERROR [ipc_ers]: Invalid regular expression '" 
        	<< server_name << "'" << std::endl;
	return 2;
    }

    boost::regex type_criteria;
    try {
	type_criteria = boost::regex( type_name );
    }
    catch ( boost::bad_expression & ex ){
	std::cerr << "ERROR [ipc_ers]: Invalid regular expression '" 
        	<< type_name << "'" << std::endl;
	return 2;
    }
      
    Parameters params;
    params.affected_applications = 0;
    params.debug_level = ( debug.flags() && CmdArg::GIVEN ) ? debug : INT_MIN;
    params.verbosity_level = ( verbosity.flags() && CmdArg::GIVEN ) ? verbosity : INT_MIN;
    
    if ( partition_name.flags() & CmdArg::GIVEN )
    {
	IPCPartition  partition( (const char*)partition_name );
        if ( (type_name.flags() && CmdArg::GIVEN) || (server_name.flags() && CmdArg::GIVEN) )
        {
	    try {
		browsePartition( partition, object_criteria, type_criteria, params, trace );
	    }
	    catch( daq::ipc::InvalidPartition & ex ) {
		ers::fatal( ex );
		return 3;
	    }
        }
        else
        {
	    ipc::partition_var pp = partition.getImplementation();
	    doAction( pp, "ipc/partition", partition.name(), params, trace );
        }
    }
    else
    {
        if ( (type_name.flags() && CmdArg::GIVEN) || (server_name.flags() && CmdArg::GIVEN) )
        {
	    browseRootPartition( object_criteria, type_criteria, params, trace );
        }
        else
        {
	    IPCPartition  partition;
	    ipc::partition_var pp = partition.getImplementation();
	    doAction( pp, "ipc/partition", partition.name(), params, trace );
        }
    }
   
    if ( params.affected_applications == 0 )
    {
	std::cerr << "\"" << type_name << "\"::" << "\"" 
        	<< server_name << "\" : No such applications" << std::endl;
    }
    return 0;
}
