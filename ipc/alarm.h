#ifndef IPC_ALARM_H
#define IPC_ALARM_H

////////////////////////////////////////////////////////////////////////////
//      alarm.h
//
//      header file for the IPC library
//
//      Sergei Kolos June 2003
//
//    description:
//	This file contains IPCAlarm class declaration
//
////////////////////////////////////////////////////////////////////////////

/*! \file alarm.h Defines the IPCAlarm class which provides a means 
 * 		  of periodic execution of some actions. 
 * \author Serguei Kolos
 * \version 1.0 
 */

#include <boost/function.hpp>
#include <boost/thread.hpp>

/*! \brief  Provides a means for periodic action execution
    \ingroup
    \sa
 */
class IPCAlarm
{
  public:
    /*!
        \var typedef bool (*Callback)( void * );
        \brief  Defines a type of the function which can be executed by the IPCAlarm.
    */
    typedef bool (*Callback)( void * );
   
    /*! Setup an action which will be executed periodically either until this object is destoryed
    	or untill the callback function will return false.
        \param  seconds 	number of seconds between different subsequent action invocations 
        \param  nanoseconds	number of nanoseconds between different subsequent action invocations
        			which is added to the number of seconds
        \param  callback	callback function which will be called periodically
     */
    explicit IPCAlarm( long seconds, long nanoseconds, const boost::function<bool ( void )> & callback );

    /*! Setup an action which will be executed periodically either until this object is destoryed
    	or untill the callback function will return false.
        \param  period 		number of seconds between different subsequent action invocations 
        \param  callback	callback function which will be called periodically
     */
    explicit IPCAlarm( double period, const boost::function<bool ( void )> & callback );

    /*! Setup an action which will be executed periodically either until this object is destoryed
    	or untill the callback function will return false.
        \param  seconds 	number of seconds between different subsequent action invocations 
        \param  nanoseconds	number of nanoseconds between different subsequent action invocations
        			which is added to the number of seconds
        \param  callback	callback function which must be of the IPCAlarm::Callback type
        \param  param		parameter which will be passed to the callback at every invocation
     */
    IPCAlarm( long seconds, long nanoseconds, IPCAlarm::Callback callback, void * param = 0 );

    /*! Setup an action which will be executed periodically either until this object is destoryed
    	or untill the callback function will return false.
        \param  period 		number of seconds between different subsequent action invocations 
        \param  callback	callback function which must be of the IPCAlarm::Callback type
        \param  param		parameter which will be passed to the callback at every invocation
     */
    IPCAlarm( double period, IPCAlarm::Callback callback, void * param = 0 );

    /*! Stops the action invocations and destroy the alarm.
        \param  
     */
    ~IPCAlarm ( );
    
    /*! Suspends action invocations.
        \sa resume() 
     */
    void suspend();

    /*! Resumes action invocations.
        \sa suspend() 
     */
    void resume();
        
  private:
    void threadBody( boost::function<bool (void)> callback, long s, long ns );

  private:
    boost::mutex  m_mutex;
    bool	  m_suspended;
    bool	  m_terminated;
    boost::thread m_thread;
};

#endif
