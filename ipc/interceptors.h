/**
*	interceptors.h
*	Declares the interface classes for the user defined interceptors.
*
*	Author:  Serguei Kolos
*	Created: 14. January 2005
*/

#ifndef IPC_INTERCEPTORS_H
#define IPC_INTERCEPTORS_H

#include <omniORB4/CORBA.h>

/*! \file interceptors.h Defines base classes user defined interceptors. 
 *  \author Serguei Kolos
 *  \version 1.0 
 */

/*! \brief This class provides interface for user specific client site interceptors.

    User must inherit his class from this one and implement virtual function. 
    This class has to be compiled to a shared library, which must have the same name
    as the interceptor class. In addition to that this shared library implementation must
    include a function (with C rather than C++ linkage) called create_XXX (where XXX 
    is the name of the interceptor class) which calls the constructor of the derived
    class and returns a pointer to the interface class. For example if there
    were an implemenation of the MyClientInterceptor, libMyClientInterceptor.so library
    would contain the compiled version of:
    \code
    extern "C" IPCClientInterceptor * create_MyClientInterceptor()
    {
	return new MyClientInterceptor();
    }
    \endcode
    
    In order to use the MyClientInterceptor one must set the following environment variable:
    \code
    > export TDAQ_IPC_CLIENT_INTERCEPTORS="MyClientInterceptor"
    \endcode
    \ingroup public
    \sa IPCServerInterceptor
 */
struct IPCClientInterceptor
{
    /*! This function must be implemented by a user specific interceptor class.
    	It is called for every remote function which is issued by the application
        which installs this interceptor.
        \param  ior		IOR reference of the target object
        \param  interface_id	Name of the remote interface which implements the function that is being invoked
        \param  operation_id	Name of the operation which is being invoked
        \param  scl		List of service contexts. For more infomation see omniORB
        			documentation.
     */
    virtual void sendRequest(	const omniIOR & ior,
    				const char * interface_id,
				const char * operation_id,
				IOP::ServiceContextList & scl ) = 0;

    virtual ~IPCClientInterceptor() { ; }
};

/*! \brief This class provides interface for user specific server site interceptors.

    User must inherit his class from this one and implement virtual function. 
    This class has to be compiled to a shared library, which must have the same name
    as the interceptor class. In addition to that this shared library implementation must
    include a function (with C rather than C++ linkage) called create_XXX (where XXX 
    is the name of the interceptor class) which calls the constructor of the derived
    class and returns a pointer to the interface class. For example if there
    were an implemenation of the MyServerInterceptor, libMyServerInterceptor.so library
    would contain the compiled version of:
    \code
    extern "C" IPCServerInterceptor * create_MyServerInterceptor()
    {
	return new MyServerInterceptor();
    }
    \endcode
    
    In order to use the MyServerInterceptor one must set the following environment variable:
    \code
    > export TDAQ_IPC_SERVER_INTERCEPTORS="MyServerInterceptor"
    \endcode
    \ingroup public
    \sa IPCClientInterceptor
 */
struct IPCServerInterceptor
{
    /*! This function must be implemented by a user specific interceptor class.
    	It is called for every remote function which is invoked on the application
        which installs this interceptor.
        \param  interface_id	Name of the remote interface which implements the function that is being invoked
        \param  operation_id	Name of the operation which is being invoked
        \param  scl		List of service contexts. For more infomation see omniORB
        			documentation.
     */
    virtual void receiveRequest( const char * interface_id,
				 const char * operation_id,
				 IOP::ServiceContextList & scl ) = 0;

    virtual ~IPCServerInterceptor() { ; }
};

#endif
