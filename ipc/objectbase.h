#ifndef IPC_OBJECTBASE_H
#define IPC_OBJECTBASE_H

/*! \file objectbase.h Defines base class for IPC objects.
 * \author Serguei Kolos
 * \version 1.0
 */

#include <mutex>

#include <ipc/objectadapter.h>
#include <ipc/partition.h>
#include <ipc/servantbase.h>

/*! \brief Base class for the IPCObject and IPCNamedObject classes. It implements an association of IPC CORBA
	servant (implemented by IPCObjectBase class) with IPC CORBA Object Adapter (implemented by the
        IPCObjectAdapter class). Both IPCObject and IPCNamedObject inherit this class.
    \ingroup public
    \sa IPCObject
    \sa IPCNamedObject
 */
template <class T, class TP = ipc::multi_thread, class PP = ipc::transient>
class IPCObjectBase :	public IPCServantBase<T>,
			public TP,
			public PP
{  
  public:
  
    virtual ~IPCObjectBase( ) {
	ERS_ASSERT_MSG( !m_active,
			"the IPCObject destructor is called, but the corresponding servant is still active\n" <<
			"to fix the problem you have to respect the following rules:\n" <<
			"    1. Always allocate your IPC based objects in the heap using the new operator\n" <<
			"    2. Always use the _destroy() method instead of the delete operator" );
	if (m_mutex) {
	    m_mutex->unlock();
	}
    }
    
    /*! This method provides the only valid way of destroying IPC objects. It must be called
    	instead of the delete operator for any object which inherits from IPCObject or IPCNamedObject.
        \param  wait_for_completion	This parameter specify whether the function has to return
        				immediately or it must wait before the operation will be completed.
                                        Note that completing the object destructor might be a long operation
                                        since it will wait for completion of all the external requests on that 
                                        object. Default is false.
     */
    virtual void _destroy( bool wait_for_completion = false )
    {
	if ( m_active ) {
	    m_adapter.remove( this );
	    m_active = false;
	}
        if ( wait_for_completion ) {
	    std::mutex * m = m_mutex;
	    this->_remove_ref();
	    m -> lock();
            m -> unlock();
	    delete m;
	}
	else {
	    m_mutex->unlock();
	    delete m_mutex;
	    m_mutex = 0;
	    this->_remove_ref();
	}
    }
    
    virtual PortableServer::POA_ptr _default_POA() {
	if ( !m_active ) {
	    m_adapter.add( this );
	    m_active = true;
	}
	return m_adapter;
    }    
    
  protected:
    IPCObjectBase( )
      : m_active( false ),
      	m_adapter( this ),
      	m_mutex( new std::mutex() )
    {
        m_mutex->lock();
    }
    
    IPCObjectBase( const IPCObjectBase * s )
      : m_active( false ),
      	m_adapter( s->m_adapter ),
      	m_mutex( new std::mutex() )
    {
        m_mutex->lock();
    }
    
  private:      
    ///////////////////////////////////////////////////
    // Disable copying
    ///////////////////////////////////////////////////
    IPCObjectBase( const IPCObjectBase & );
    IPCObjectBase & operator=( const IPCObjectBase & );
    
  private:      
    bool		m_active;
    IPCObjectAdapter	m_adapter;
    std::mutex * 	m_mutex;
};

#endif
