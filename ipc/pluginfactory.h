// //////////////////////////////////////////////////////////////////////
//
//  Author:  G.J.Crone, University College London
//
//  Modified: 14.Jan 2005    John.Erik.Sloper@cern.ch
//  Modified: 19.Jan 2005    Serguei.Kolos@cern.ch
//
//  Based on PluginFacory.h in DAQ/DataFlow/ROSUtilities package
//  (author: Giovanna Lehmann)
//  Temporary solution untill common classes will be available
/////////////////////////////////////////////////////////////////////////


#ifndef IPC_PLUGINFACTORY_H
#define IPC_PLUGINFACTORY_H

#ifndef __rtems__
#include <dlfcn.h>
#endif

#include <cstring>
#include <string>
#include <map>
#include <memory>

#include <ipc/exceptions.h>

/**
@brief  Template class to load shared libraries containing derived 
 classes of given name and later call constructor.

 Each shared library implementation must include a function (with C
 rather than C++ linkage) called create_XXX (where XXX is the name of
 the derived class) which calls the constructor of the derived class
 and returns a pointer to the interface class. For example if there
 were an implementation of the MyClientInterceptor,
 libMyClientInterceptor.so would contain the compiled version of:

 <PRE>
 extern "C"<BR>
 daq::ipc::ClientInterceptor *<BR>
 create_MyClientInterceptor()<BR>
 {<BR>
    return (new MyClientInterceptor());<BR>
 }<BR>
</PRE>
*/

template <class T> class IPCSingleton;

template <class T>
class IPCPluginFactory
{    
    template <class> friend class IPCSingleton;
    
    typedef T * (*CreateFunction)();
    typedef void (*InitFunction)();
    
    class SharedLibrary
    {
      public:
	/**  Load the shared library and locate the create function.
	Store a pointer to the create function in our map indexed by
	the library name.
	@param name Name of the class to be loaded (library name excluding
	lib prefix and .so suffix).
	*/
	SharedLibrary( const std::string & name );
    
	/** Unload the dynamic library containing class name and remove it from our map.  */
	~SharedLibrary();
    
	T * create()
	{ 
            if ( !m_create_function ) {
            	throw daq::ipc::PluginException( ERS_HERE, m_name, "symbol create_" + m_name + " is not found" );
            }
	    return m_create_function();
        }
    
	void init()
	{
            if ( !m_init_function ) {
            	throw daq::ipc::PluginException( ERS_HERE, m_name, "symbol init_" + m_name + " is not found" );
            }
	    return m_init_function();
        }
    
      private:
	std::string	m_name;
        void *		m_handle;
	CreateFunction	m_create_function;
        InitFunction	m_init_function;
    };
    
    typedef std::map< std::string, std::shared_ptr<SharedLibrary>> LibMap;
    
    LibMap		m_libraries;
    
    /** Constructor is private to use this class as singleton only.
     */
    IPCPluginFactory()
    { ; }
      
    void destroy() 
    { delete this; }
    
  public:
    /** Find the create function for plugin \a name, call it and return
	a pointer to the object it created.
	@param name Name of the library to be loaded. The library must provide a function with C linkage 
        		called "create_"<name> which will be called to perform the necessary initialization.
    */
    T * create( const std::string & name );    
    
    /** Find the initialization function for plugin \a name and call it.
	@param name Name of the library to be loaded. The library must provide a function with C linkage 
        		called "create_"<name> which will be called to perform the necessary initialization.
    */
    void init( const std::string & name );    
    
    /** Just loads the \a name plugin.
	@param name Name of the library to be loaded. The library must provide a function with C linkage 
        		called "create_"<name> which will be called to perform the necessary initialization.
    */
    std::shared_ptr<SharedLibrary> load( const std::string & name );
};

template <class T>
IPCPluginFactory<T>::SharedLibrary::SharedLibrary( const std::string & name )
  : m_name( name ),
    m_handle( 0 ),
    m_create_function( 0 ),
    m_init_function( 0 )
{
#ifndef __rtems__
    std::string library = "lib" + name + ".so" ;
	
    m_handle = dlopen( library.c_str(), RTLD_LAZY|RTLD_GLOBAL );

    if ( !m_handle )
    {	   
	throw daq::ipc::PluginException( ERS_HERE, name, dlerror() );
    }
	
    std::string cname = "create_" + name;
    void * p = dlsym( m_handle, cname.c_str() );
    std::memcpy(&m_create_function, &p, sizeof(p));
    
    std::string iname = "init_" + name;
    p = dlsym( m_handle, iname.c_str() );
    std::memcpy(&m_init_function, &p, sizeof(p));
#endif
}

template <class T>
IPCPluginFactory<T>::SharedLibrary::~SharedLibrary( )
{
    ////////////////////////////////////////////////////////////////////
    // Library should be unloaded, but with some compilers (e.g gcc323)
    // this results in crash of program, because it is not recognized
    // that the library is still in use
    ////////////////////////////////////////////////////////////////////
    
    //dlclose( handle_ );
}
  
template <class T>
T *
IPCPluginFactory<T>::create( const std::string & name )
{
    std::shared_ptr<SharedLibrary> lib = load( name );
    return lib->create();
}

template <class T>
void
IPCPluginFactory<T>::init( const std::string & name )
{
    std::shared_ptr<SharedLibrary> lib = load( name );
    lib->init();
}

template <class T>
std::shared_ptr<typename IPCPluginFactory<T>::SharedLibrary>
IPCPluginFactory<T>::load( const std::string & name )
{
    std::shared_ptr<SharedLibrary> lib;
    typename LibMap::iterator it = m_libraries.find(name);
    if ( it == m_libraries.end() )
    {
	lib = std::shared_ptr<SharedLibrary>(new SharedLibrary( name ));
	m_libraries[name] = lib;
    }
    else
    {
	lib = it->second;
    }
    return lib;
}

#endif
