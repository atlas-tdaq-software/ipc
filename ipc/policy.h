#ifndef IPC_POLICY_H
#define IPC_POLICY_H

///////////////////////////////////////////////////////////////////////////
//      policy.h
//
//      header file for the IPC library
//
//      Sergei Kolos, September 2003
//
//      description:
///////////////////////////////////////////////////////////////////////////

#include <string>
#include <sstream>

#include <boost/bind/bind.hpp>
#include <boost/lexical_cast.hpp>
#include <boost/noncopyable.hpp>

#include <ipc/cache.h>
#include <ipc/util.h>
#include <ipc/singleton.h>

/*! \file policy.h Defines policy classes for the IPCPartition functions. 
 * \author Serguei Kolos
 * \version 1.0 
 */

/*! \namespace ipc
 *  This is a wrapping namespace for the IPC policy classes.
 */

namespace ipc
{
typedef CORBA::Object_ptr (*Fun1)(const std::string&);
typedef CORBA::Object_ptr (*Fun3)(const std::string&, const std::string&,
	const std::string&);

/*! \brief This class provides implementation of the cache policy for some
 functions of the IPCPartition class.
 Using this policy enforces the usage internal cache for the remote object references.
 \ingroup public
 \sa no_cache
 */
template<class T, template<class > class VP>
struct use_cache
{
    template<Fun1 fun>
    static typename T::_ptr_type resolve(const std::string & name)
    {
	CORBA::Object_var obj = IPCSingleton<IPCCache>::instance().find(name,
		boost::bind(fun, name));
	typename T::_var_type result = VP<T>::validate(obj);
	return T::_duplicate(result);
    }

    template<Fun3 fun>
    static typename T::_ptr_type resolve(const std::string & n1,
	    const std::string & n2, const std::string & n3)
    {
	CORBA::Object_var obj = IPCSingleton<IPCCache>::instance().find(
		n1 + '/' + n2 + '@' + n3, boost::bind(fun, n1, n2, n3));
	typename T::_var_type result = VP<T>::validate(obj);
	return T::_duplicate(result);
    }
};

/*! \brief This class provides implementation of the cache policy for some functions
 of the IPCPartition class.
 Using this policy avoids the usage internal cache for the remote object references.
 \ingroup public
 \sa no_cache
 */
template<class T, template<class > class VP>
struct no_cache
{
    template<Fun1 fun>
    static typename T::_ptr_type resolve(const std::string & name)
    {
	CORBA::Object_var obj = fun(name);
	return VP<T>::validate(obj);
    }

    template<Fun3 fun>
    static typename T::_ptr_type resolve(const std::string & n1,
	    const std::string & n2, const std::string & n3)
    {
	CORBA::Object_var obj = fun(n1, n2, n3);
	return VP<T>::validate(obj);
    }
};

/*! \brief This class provides implementation of the validation policy for some functions of the IPCPartition class.
 Using this policy implies that the object reference will be validated using the unchecked_narrow function, which
 checks if the object reference represents the object of a given type. This function does a local
 check only, i.e. it is guaranteed that no remote communication will be done at that point. This
 function does not guarantee that remote object, which is referenced by the given object ref, exists.
 \ingroup public
 \sa no_cache
 */
template<class T>
struct unchecked_narrow
{
    static typename T::_ptr_type validate(CORBA::Object_ptr obj)
    {
	return T::_unchecked_narrow(obj);
    }
};

/*! \brief This class provides implementation of the validation policy for some functions of the IPCPartition class.
 Using this policy implies that the object reference will be validated using the narrow function, which
 checks if the object reference represents the object of a given type. This function may
 require to perform remote invocation on the given object. This function
 does not guarantee that remote object, which is referenced by the given object ref, exists.
 \ingroup public
 \sa no_cache
 */
template<class T>
struct narrow
{
    static typename T::_ptr_type validate(CORBA::Object_ptr obj)
    {
	try {
	    return T::_narrow(obj);
	}
	catch (...) {
	    return T::_nil();
	}
    }
};

/*! \brief This class provides implementation of the validation policy for some functions of the IPCPartition class.
 Using this policy implies that the object reference will be validated by invoking the _non_existent function
 on it. This function provides a guarantee that remote object, which is referenced by the
 given object ref, exists.
 \ingroup public
 \sa no_cache
 */
template<class T>
struct non_existent
{
    static typename T::_ptr_type validate(CORBA::Object_ptr obj)
    {
	typename T::_var_type result = T::_nil();
	try {
	    result = T::_narrow(obj);
	    if (CORBA::is_nil(result) || result->_non_existent())
		return T::_nil();
	}
	catch (...) {
	    return T::_nil();
	}
	return T::_duplicate(result);
    }
};

/*! \brief This class provides implementation of the threaded policy for the IPCEntity and IPCNamedObject classes.
 Using this policy implies that all the remote requests for the given object will be serialized, i.e. executed in the
 single thread.
 \ingroup public
 \sa no_cache
 */
struct single_thread
{
    PortableServer::ThreadPolicyValue thread_policy()
    {
	return PortableServer::SINGLE_THREAD_MODEL;
    }

    std::string poa_id_suffix() const
    {
	return boost::lexical_cast<std::string>(this);
    }
};

/*! \brief This class provides implementation of the threaded policy for the IPCEntity and IPCNamedObject classes.
 Using this policy implies that remote requests for the given object can be executed concurrently by several threads.
 \ingroup public
 \sa no_cache
 */
struct multi_thread
{
    PortableServer::ThreadPolicyValue thread_policy()
    {
	return PortableServer::ORB_CTRL_MODEL;
    }

    std::string poa_id_suffix() const
    {
	return "";
    }
};

/*! \brief This class provides implementation of the persistence policy for the IPCNamedObject class.
 \ingroup public
 */
struct transient
{
    PortableServer::LifespanPolicyValue lifespan_policy()
    {
	return PortableServer::TRANSIENT;
    }

    virtual std::string unique_id() const
    {
        auto ns = std::chrono::duration_cast<std::chrono::nanoseconds>(
                std::chrono::high_resolution_clock::now().time_since_epoch()).count();
        return boost::lexical_cast<std::string>(this)
	        + boost::lexical_cast<std::string>(ns);
    }
    
    virtual ~transient()
    {
	;
    }
};

/*! \brief This class provides implementation of the persistence policy for the IPCNamedObject class.
 \ingroup public
 */
struct persistent
{
    PortableServer::LifespanPolicyValue lifespan_policy()
    {
	return PortableServer::PERSISTENT;
    }

    virtual std::string unique_id() const = 0;

    virtual ~persistent()
    {
	;
    }
};
}

#endif
