////////////////////////////////////////////////////////////////////////////
//      manager.h
//
//      Implementation of the IPC proxy application
//
//      Sergei Kolos August 2007
//
//      description:
//              This file contains declaration of the IPC proxy manager
//
////////////////////////////////////////////////////////////////////////////

#ifndef IPC_PROXY_MANAGER_H
#define IPC_PROXY_MANAGER_H

#include <list>
#include <map>

#include <boost/thread/mutex.hpp>

#include <ipc/partition.h>
#include <ipc/proxy/objectbase.h>
#include <ipc/proxy/factorybase.h>

class IPCProxyManager
{
    template <class> friend class IPCSingleton;
  
  public:
    
    ~IPCProxyManager();
    
    CORBA::Object_ptr createProxy( const std::string & interface, const std::string & ior );
    CORBA::Object_ptr createProxy( const IPCPartition & partition, const std::string & name,
            const std::string & interface, CORBA::Object_ptr remote );
    void init( );
    void init( const std::string & plugins );
    void getRegisteredInterfaces( std::list<std::string> & list );
    void registerFactory( IPCProxyFactoryBase * factory ) {
	ERS_DEBUG( 1, "registering the factory of the '" << factory->interfaceId() << "' interface" );
	boost::mutex::scoped_lock lock( mutex_ );
	factories_.insert( std::make_pair( factory->interfaceId(), factory ) );    
    }
    
    void cleanup( );
    
  private:
    IPCProxyManager() { ; }
    
    void destroy(); 
    
  private:
    typedef std::map<std::string, IPCProxyFactoryBase * > ProxyFactories;
    typedef std::map<std::string, IPCProxyObjectBase * >  ProxyObjects;
    
    boost::mutex	mutex_;
    ProxyFactories	factories_;
    ProxyObjects	objects_;
};

#endif
