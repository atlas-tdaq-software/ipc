/////////////////////////////////////////////////////////////////////////////////////////
//      object.h
//
//      Implementation of the IPC proxy application
//
//      Sergei Kolos August 2007
//
//      description:
//              This file contains implementation of the IPC proxy object interface
//
/////////////////////////////////////////////////////////////////////////////////////////

#ifndef IPC_PROXY_NAMED_OBJECT_H
#define IPC_PROXY_NAMED_OBJECT_H

#include <type_traits>

#include <ipc/object.h>
#include <ipc/exceptions.h>
#include <ipc/servant.h>
#include <ipc/proxy/object.h>


template<class I, class POA_I, template<class > class POA_I_tie>
class IPCProxyNamedObject: public IPCProxyObject<I, POA_I, POA_I_tie>,
                           public IPCServant
{
public:
    IPCProxyNamedObject(const IPCPartition & partition,
            const std::string & name, CORBA::Object_ptr remote)
    : IPCProxyObject<I, POA_I, POA_I_tie>(remote),
      IPCServant(partition, name)
    { }

    /////////////////////////////////////////////////////////////////////////////////////
    // Override ipc::servant methods to have them applied for the proxy object itself
    /////////////////////////////////////////////////////////////////////////////////////
    void ping() {
        IPCServant::ping();
    }

    void shutdown() {
        IPCServant::shutdown();
    }

    void test(const char *s) {
        IPCServant::test(s);
    }

    CORBA::Short debug_level() {
        return IPCServant::debug_level();
    }

    void debug_level( CORBA::Short new_level ) {
        IPCServant::debug_level(new_level);
    }

    CORBA::Short verbosity_level() {
        return IPCServant::verbosity_level();
    }

    void verbosity_level( CORBA::Short new_level ) {
        return IPCServant::verbosity_level(new_level);
    }

    ipc::servant::ApplicationContext * app_context() {
        return IPCServant::app_context();
    }

    ipc::servant::PartitionContext * partition_context() {
        return IPCServant::partition_context();
    }
};

#endif
