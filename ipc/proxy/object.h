/////////////////////////////////////////////////////////////////////////////////////////
//      object.h
//
//      Implementation of the IPC proxy application
//
//      Sergei Kolos August 2007
//
//      description:
//              This file contains implementation of the IPC proxy object interface
//
/////////////////////////////////////////////////////////////////////////////////////////

#ifndef IPC_PROXY_OBJECT_H
#define IPC_PROXY_OBJECT_H

#include <type_traits>

#include <ipc/object.h>
#include <ipc/exceptions.h>
#include <ipc/proxy/objectbase.h>

namespace ipc
{
// this wrapper class converts protected destructor of the class T
// to the public one of the class wrapper<T>
  template<class T>
  struct wrapper: public T
  {};
}

template<class I, class POA_I, template<class > class POA_I_tie>
class IPCProxyObject: public IPCProxyObjectBase,
		      public IPCObject<POA_I, ipc::multi_thread, ipc::transient>,
		      public POA_I_tie<ipc::wrapper<typename std::remove_pointer<typename I::_ptr_type>::type>>
{
protected:
    typedef ipc::wrapper<typename std::remove_pointer<typename I::_ptr_type>::type> TargetType;

    typename I::_var_type target_;
    typename I::_var_type self_;

public:
    IPCProxyObject(CORBA::Object_ptr ref)
    : POA_I_tie<TargetType>( reinterpret_cast<TargetType*>( I::_narrow( ref ) ) )
    {
        target_ = this -> _tied_object();
	if ( CORBA::is_nil( target_ ) )
	{
	    ERS_DEBUG( 1, "Got nil reference to the '" << ref << "' remote object" );
	    throw CORBA::INV_OBJREF( 0, CORBA::COMPLETED_NO );
	}
	this->_is_owner(false);
        self_ = this -> _this();
    }

    IPCProxyObject(const std::string & ior)
    : IPCProxyObject( createTarget(ior) )
    { }

private:
    static typename I::_ptr_type createTarget(const std::string & ior)
    {
	try {
            return I::_narrow( IPCCore::stringToObject( ior ) );
	}
	catch( CORBA::SystemException & ex ) {
	    ERS_DEBUG( 1, "Cannot contact the '" << ior
		    << "' remote object, got '" << ex << "' exception" );
	    throw;
	}
    }

    PortableServer::POA_ptr _default_POA() override {
        return IPCObject<POA_I,ipc::multi_thread,ipc::transient>::_default_POA();
    }

    /////////////////////////////////////////////////////////////////////////////////////
    // Implement pure virtual methods of the base proxy class
    /////////////////////////////////////////////////////////////////////////////////////
    CORBA::Object_ptr __reference__() final
    {
	return self_;
    }

    bool __target_exists__() final
    {
	try {
	    if ( CORBA::is_nil(target_) || target_ -> _non_existent() )
	    return false;
	}
	catch(...) {
	    return false;
	}

	return true;
    }

    void __destroy__() final
    {
	this -> _destroy();
    }
};

#endif
