package ipc;

/**
 * Thrown to indicate that requested object is not registered with the given partition
 * 
 * @author Serguei Kolos
 * @since 20/01/13
 */
public class InvalidObjectException extends ers.Issue
{
    /**
     * Constructs an exception object with the specified parameters.
     * 
     * @param pname	partition name
     * @param type_name	object type name
     * @param name	object name
     */
    public InvalidObjectException(String pname, String type_name, String name, Exception cause)
    {
	super("Object '" 
		+ (type_name == null 	? name 
					: name == null 	? type_name 
							: type_name + "/" + name)
		+ "' is not registered in the '" + pname + "' partition", 
		cause);
    }
}
