package ipc;

import ers.Issue;

/**
 * Thrown to indicate that the given object reference does not correspond
 * to any valid CORBA object.
 * 
 * @author Serguei Kolos
 * @since 20/01/13
 */
public class InvalidReferenceException extends Issue
{
    public InvalidReferenceException(String message)
    {
	super(message);
    }

    public InvalidReferenceException(Exception cause)
    {
	super("Invalid object reference", cause);
    }
}
