package ipc;

import org.omg.PortableInterceptor.ORBInitInfo;

public class ORBInitializer extends org.omg.CORBA.LocalObject implements
	org.omg.PortableInterceptor.ORBInitializer
{
    public void pre_init(ORBInitInfo info)
    {
    }

    public void post_init(ORBInitInfo info)
    {
	try {
	    info.add_client_request_interceptor(new ClientInterceptor(info));
	}
	catch (Exception e) {
	    System.err.println("WARNING in [ORBInitializer.post_init] : (" + e
		    + ")");
	}
    }
}
