package ipc;

import org.omg.CosNaming.*;
import java.util.Vector;

/**
 * @author Serguei Kolos
 * This class can be used to enumerate all known IPC objects of a certain type.
 * A constructor of this class retrieves object references from the
 * IPC Naming Service. There are two types of constructors. One is taking 
 * the Class<T> parameter to identify enumerated objects type, while the
 * others take the literal type name. The latter have to be used when 
 * respective object types are not known at compile time. 
 * <p>This class provides two ways of iterating over its elements:
 * <ul>
 *	<li>by implementing the java.util.Enumeration interface<Element<T>>
 * 	<li>by extending the Vector<Element<T>> class
 * </ul>
 * @param <Type> type of IPC objects. A valid type is a Java 
 * 		<tt>abstract interface</tt> which is generated from the
 * 		respective IDL <tt>interface</tt> declaration.
 * For example to enumerate all objects of the following IDL interface in the 
 * "ATLAS" partition:
 * <pre>
 * {@code
 * 	module is {
 * 		interface repository { ... };
 * 	};
 * }
 * </pre>
 * one has to use the following code:
 * <pre>
 * {@code
 * 	ObjectEnumeration<is.repository> enumeration 
 * 		= new ObjectEnumeration<is.repository>(new Partition("ATLAS"), is.repository.class);
 * }
 * </pre>
 * 
 * @see Element<Type>	
 */
public class ObjectEnumeration<Type extends ipc.servant> 
	extends Vector<Element<Type>>
	implements java.util.Enumeration<Element<Type>>
{
    private int pos;

    /**
     * Constructs objects enumeration for the given partition.
     * @param p			partition
     * @param type		objects type
     * @param check_validity	if the value is true then the final enumeration will contain
     * 				only alive objects, otherwise all references of the given type,
     * 				from the Naming Service will be used. In the latter case some 
     * 				of them may point to non-existing objects.
     * 
     * @throws InvalidPartitionException - if the given partition does not exist
     */
    public ObjectEnumeration(Partition p, Class<Type> type,
	    boolean check_validity) throws InvalidPartitionException
    {
	Binding[] bl = NamingService.listBindings(p.getName(), TypeHelper.name(type));
	if (bl != null) {
	    for (int i = 0; i < bl.length; i++) {
		try {
		    Type obj = p.lookup(type, bl[i].binding_name[0].id);
		    if (obj != null && (!check_validity || obj._non_existent() == false)) {
			add(new Element<Type>(bl[i].binding_name[0].id, obj));
		    }
		}
		catch (Exception e) {
		    continue;
		}
	    }
	}
	pos = 0;
    }

    /**
     * Constructs objects enumeration for the given partition. This constructor should be
     * used only when enumerated objects type is not known during compilation. In this case
     * one has to use either org.omg.Corba.Object or ipc.servant as <tt>Type</tt> parameter.
     * @param p			partition
     * @param type_name		name of the objects type
     * @param check_validity	if the value is true then the final enumeration will contain
     * 				only alive objects, otherwise all references of the given type,
     * 				from the Naming Service will be used. In the latter case some 
     * 				of them may point to non-existing objects.
     * 
     * @throws InvalidPartitionException - if the given partition does not exist
     */
    public ObjectEnumeration(Partition p, String type_name,
	    boolean check_validity) throws InvalidPartitionException
    {
	Binding[] bl = NamingService.listBindings(p.getName(), type_name);
	if (bl != null) {
	    for (int i = 0; i < bl.length; i++) {
		try {
		    ipc.servant obj = p.lookup(type_name, bl[i].binding_name[0].id);
		    if (obj != null && (!check_validity || obj._non_existent() == false)) {
			add(new Element<Type>(bl[i].binding_name[0].id, (Type)obj));
		    }
		}
		catch (Exception e) {
		    continue;
		}
	    }
	}
	pos = 0;
    }
    
    /**
     * Constructs objects enumeration for the given partition. This constructor 
     * does the objects validity check, so only existing objects will be included
     * to the enumeration.
     * @param p		partition
     * @param type	objects type
     * 
     * @throws InvalidPartitionException - if the given partition does not exist
     */
    public ObjectEnumeration(Partition p, Class<Type> type)
	    throws InvalidPartitionException
    {
	this(p, type, true);
    }

    /**
     * Constructs objects enumeration for the given partition. This constructor has to be
     * used only when enumerated objects type is not known during compilation. In this case
     * one has to use either org.omg.Corba.Object or ipc.servant as <tt>T</tt> type.
     * <p>This constructor does the objects validity check, so only existing objects will be 
     * included to the enumeration.
     * @param p			partition
     * @param type_name		name of the objects type
     * @param check_validity	if the value is true then the final enumeration will contain
     * 				only alive objects, otherwise all references of the given type,
     * 				from the Naming Service will be used. In the latter case some 
     * 				of them may point to non-existing objects.
     * 
     * @throws InvalidPartitionException	if the given partition does not exist
     */
    public ObjectEnumeration(Partition p, String type_name)
	    throws InvalidPartitionException
    {
	this(p, type_name, true);
    }

    /**
     * @return -  if and only if this enumeration object contains 
     * 		at least one more element to provide; false otherwise.
     */
    public boolean hasMoreElements()
    {
	return (pos < size());
    }

    /**
     * Returns the next element of this enumeration if this enumeration 
     * object has at least one more element to provide.
     * @return the next element of this enumeration.
     * 
     * @throws NoSuchElementException	if no more elements exist.     
     */
    public Element<Type> nextElement()
    {
	try {
	    return get(pos++);
	}
	catch(Exception e) {
	    throw new java.util.NoSuchElementException();
	}
    }

    /**
     * Resets the enumeration's current position <i>before</i> the first element, 
     * i.e. to the state which this object had just after construction.
     */
    public void reset()
    {
	pos = 0;
    }
}
