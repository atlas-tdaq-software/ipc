package ipc;

import java.util.Date;
import java.io.IOException;
import java.security.GeneralSecurityException;

import java.net.InetAddress;
import java.net.ServerSocket;
import javax.net.ServerSocketFactory;
import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLServerSocket;

import sun.security.x509.*;

import java.security.*;
import java.security.cert.*;
import java.security.spec.ECGenParameterSpec;
import java.security.cert.Certificate;

import org.jacorb.config.Configurable;
import org.jacorb.config.Configuration;
import org.jacorb.config.ConfigurationException;


public class SSLServerSocketFactory
    extends org.jacorb.security.ssl.sun_jsse.SSLRandom
    implements org.jacorb.orb.factory.ServerSocketFactory, Configurable
{
    private ServerSocketFactory factory = null;
    private String password = "tdaqpw";

    // Create a key store with self signed certificate and key
    private KeyStore generateCertificate()
        throws GeneralSecurityException, IOException
    {
        String dn = "CN=Java, OU=TDAQ, O=ATLAS";
        String algorithm = "SHA256withECDSA";

        // Generate key, EC P-256
        KeyPairGenerator keyPairGenerator = KeyPairGenerator.getInstance("EC");
        ECGenParameterSpec spec = new ECGenParameterSpec("secp256r1");
        keyPairGenerator.initialize(spec);
        KeyPair pair = keyPairGenerator.generateKeyPair();

        // Prepare certificate, 1 year validity, fixed name
        PrivateKey privkey = pair.getPrivate();
        X509CertInfo info = new X509CertInfo();
        Date from = new Date();
        Date to = new Date(from.getTime() + 365 * 86400000l);
        CertificateValidity interval = new CertificateValidity(from, to);
        X500Name owner = new X500Name(dn);

        info.set(X509CertInfo.VALIDITY, interval);
        info.set(X509CertInfo.SERIAL_NUMBER, new CertificateSerialNumber(1));
        info.set(X509CertInfo.SUBJECT, owner);
        info.set(X509CertInfo.ISSUER, owner);
        info.set(X509CertInfo.KEY, new CertificateX509Key(pair.getPublic()));
        info.set(X509CertInfo.VERSION, new CertificateVersion(CertificateVersion.V3));
        AlgorithmId algo = new AlgorithmId(AlgorithmId.md5WithRSAEncryption_oid);
        info.set(X509CertInfo.ALGORITHM_ID, new CertificateAlgorithmId(algo));

        // Sign the cert to identify the algorithm that's used.
        // This weird code seems to originate in the original
        // keytool source, so...
        X509CertImpl cert = new X509CertImpl(info);
        cert.sign(privkey, algorithm);

        // Update the algorith, and resign.
        algo = (AlgorithmId) cert.get(X509CertImpl.SIG_ALG);
        info.set(CertificateAlgorithmId.NAME + "." + CertificateAlgorithmId.ALGORITHM, algo);
        cert = new X509CertImpl(info);
        cert.sign(privkey, algorithm);

        // Store credentials in in-memory key store
        KeyStore ks = KeyStore.getInstance(KeyStore.getDefaultType());
        ks.load(null, null);
        ks.setKeyEntry("tdaq", privkey, password.toCharArray(), new Certificate[] { cert });

        return ks;
    }

    public void configure(Configuration configuration)
        throws ConfigurationException
    {
        super.configure(configuration);

        final org.jacorb.config.Configuration config = (org.jacorb.config.Configuration) configuration;

        try
        {
            factory = createServerSocketFactory();
        }
        catch(Exception e)
        {
            logger.warn("Unable to create ServerSocketFactory : {}", e.getMessage (), e);

            throw new ConfigurationException("Unable to create ServerSocketFactory!", e);
        }
    }

    private ServerSocketFactory createServerSocketFactory()
        throws IOException, java.security.GeneralSecurityException
    {
        KeyManagerFactory kmf = KeyManagerFactory.getInstance("SunX509");

        KeyStore ks = generateCertificate();
        kmf.init(ks, password.toCharArray());

        SSLContext ctx = SSLContext.getInstance( "TLS" );
        ctx.init( kmf.getKeyManagers(),
                  new TrustManager[] {},
                  getSecureRandom());

        return ctx.getServerSocketFactory();
    }

    public ServerSocket createServerSocket( int port )
        throws IOException
    {
        SSLServerSocket s = (SSLServerSocket)
            factory.createServerSocket( port );

        return s;
    }


    public ServerSocket createServerSocket( int port, int backlog )
        throws IOException
    {
        SSLServerSocket s = (SSLServerSocket)
            factory.createServerSocket( port, backlog );

        return s;
    }

    public ServerSocket createServerSocket (int port,
                                            int backlog,
                                            InetAddress ifAddress)
        throws IOException
    {
        SSLServerSocket s = (SSLServerSocket)
            factory.createServerSocket( port, backlog, ifAddress );

        return s;
    }

}
