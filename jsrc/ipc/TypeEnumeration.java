package ipc;

import org.omg.CosNaming.*;
import java.util.Vector;

/**
 * @author Serguei Kolos
 * This class can be used to enumerate all known IPC types in a certain partition.
 * A constructor of this class retrieves objects types from the IPC Naming Service
 * and put their names to this enumeration. Type names corresponds to service
 * specific the IDL <tt>interface</tt> names. For example the following IDL interface:
 * <pre>
 * {@code
 * 	module is {
 * 		interface repository { };
 * 	};
 * }
 * </pre>
 * will be represented by the <tt>"is/repository"</tt> string in the type enumeration.
 * <p>This class provides two ways of iterating over its elements:
 * <ul>
 *	<li>by implementing the java.util.Enumeration interface<Partition>
 * 	<li>by extending the Vector<Partition> class
 * </ul>	
 */
public class TypeEnumeration extends Vector<String>
	implements java.util.Enumeration<String>
{
    private int pos;

    /**
     * Enumerates objects types in the given partition.
     * @param p	partition
     * 
     * @throws InvalidPartitionException - if the given partition does not exist
     */
    public TypeEnumeration(Partition p) throws InvalidPartitionException
    {
	Binding[] bl = NamingService.listBindings(p.getName(), null);
	if (bl != null) {
	    for (int i = 0; i < bl.length; i++) {
		if (bl[i].binding_type == BindingType.ncontext) {
		    add(bl[i].binding_name[0].id);
		}
	    }
	}
	pos = 0;
    }

    /**
     * @return	if and only if this enumeration object contains 
     * 		at least one more element to provide; false otherwise.
     */
    public boolean hasMoreElements()
    {
	return (pos < size());
    }

    /**
     * Returns the next element of this enumeration if this enumeration 
     * object has at least one more element to provide.
     * @return the next element of this enumeration.
     * 
     * @throws NoSuchElementException	if no more elements exist.     
     */
    public String nextElement()
    {
	try {
	    return get(pos++);
	}
	catch(Exception e) {
	    throw new java.util.NoSuchElementException();
	}
    }

    /**
     * Resets the enumeration's current position <i>before</i> the first element, 
     * i.e. to the state which this object had just after construction.
     */
    public void reset()
    {
	pos = 0;
    }

}
