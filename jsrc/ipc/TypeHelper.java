package ipc;

public abstract class TypeHelper
{
    public static <Type extends org.omg.CORBA.Object> Type stringToObject(
	    Class<Type> type, String ior) throws InvalidReferenceException
    {
	return TypeHelper.narrow(type, Core.stringToObject(ior));
    }

    public static <Type extends org.omg.CORBA.Object> Type narrow(
	    Class<Type> type, org.omg.CORBA.Object object)
	    throws InvalidReferenceException
    {
	try {
	    java.lang.Class<?> helper = Class
		    .forName(type.getName() + "Helper");
	    java.lang.reflect.Method method = helper.getMethod("narrow",
		    org.omg.CORBA.Object.class);

	    return type.cast(method.invoke(null, object));
	}
	catch (java.lang.Exception e) {
	    throw new InvalidReferenceException(e);
	}
    }

    static <T extends org.omg.CORBA.Object> String name(Class<T> type)
    {
	return type.getName().replace('.', '/');
    }
}
