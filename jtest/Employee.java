import ipc.*;

class Employee extends Person<ipc.employee> implements employeeOperations
{
    private final short salary;

    Employee(Partition partition, String name, short age, short salary)
    {
	super(partition, name, age);
	this.salary = salary;
    }

    public short Salary()
    {
	return salary;
    }

    public void subscribe(ipc.callback cb, short interval)
    {
    }
}
