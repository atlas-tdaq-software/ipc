#!/bin/sh
#################################################################
#
#	Script to run the mirroring partition
#	Created by Sergei Kolos; 10.05.08
#
#################################################################

if test $# -lt 1 ; then
    echo "Usage: run_mirror_partition.sh <master_partition_name> [log_directory=/tmp] [ipc_server_port_number=12345]"
    exit 1
fi

if test $# -lt 2 ; then
	log_dir="/tmp/$1"
else
	log_dir=$2
fi

if test $# -lt 3 ; then
	port_number=12345
else
	port_number=$3
fi

mkdir -p ${log_dir}

start_partition()
{        
    output_file=$3/ipc_server_$1.out
    error_file=$3/ipc_server_$1.err
    rm -f ${output_file}
    rm -f ${error_file}
    
    ipc_server -p $1 -m -P $2 > ${output_file} 2> ${error_file} &

    echo -n "Starting '$1' IPC server ... "

    count=0
    while test ! -s ${output_file} -a ! -s ${error_file} -a ${count} -lt 30
    do
	sleep 1
	count=`expr ${count} + 1`
    done

    if test -s ${error_file}
    then
	cat ${error_file}
	AlreadyExist=`grep "Already exist" ${error_file}`
	JustWarning=`grep "WARNING" ${error_file}`
	if test -n "$AlreadyExist" -o -n "$JustWarning"
	then
	    echo "OK: Server running"
	else
	    echo "Failed!"
	    echo "(Server was not started)"
	    exit 1
	fi
    else
	if test ! -s ${output_file}
	then
	    echo "Failed!"
	    echo "(Server was not started)"
	    exit 1
	else
	    echo "Ok"
	fi
    fi
    echo "----------------------------------------------------------------------"
}
trap deactivate 1 2 3 4 5 6 7 8 10 12 13 14 15

#############################################
# Start server
#############################################
start_is_server()
{    
    output_file=$3/is_server_$2.out
    error_file=$3/is_server_$2.err
    rm -f ${output_file}
    rm -f ${error_file}

    echo -n "Starting '$2' IS server ... "
    is_server -p $1 -n $2 > ${output_file} 2> ${error_file} &
    
    count=0
    while test ! -s ${output_file} -a ! -s ${error_file} -a ${count} -lt 30
    do
	sleep 1
	count=`expr ${count} + 1`
    done

    if test -s ${error_file}
    then
	Fatal=`grep "FATAL" ${error_file}`
	if test -n "$Fatal"
	then
	    echo "FAILED"
	    cat ${error_file}
	    exit 1
	fi
    fi
    if test -s ${output_file}
    then
    	echo "Ok"
    else
	echo "FAILED"
	exit 2
    fi
    echo "----------------------------------------------------------------------"
}
trap deactivate 1 2 3 4 5 6 7 8 10 12 13 14 15


#############################################
# Start server
#############################################
start_rdb_server()
{    
    output_file=$3/rdb_server_$2.out
    error_file=$3/rdb_server_$2.err
    rm -f ${output_file}
    rm -f ${error_file}

    echo -n "Starting '$2' RDB server ... "
    rdb_server -p $1 -d $2 > ${output_file} 2> ${error_file} &
    
    count=0
    while test ! -s ${output_file} -a ! -s ${error_file} -a ${count} -lt 3
    do
	sleep 1
	count=`expr ${count} + 1`
    done
    
    echo "Ok"

    echo "----------------------------------------------------------------------"
}
trap deactivate 1 2 3 4 5 6 7 8 10 12 13 14 15

#############################################
# Start server
#############################################
start_mrs_server()
{    
    output_file=$2/mrs_server.out
    error_file=$2/mrs_server.err
    rm -f ${output_file}
    rm -f ${error_file}

    echo -n "Starting MRS server ... "
    mrs_server -p $1 > ${output_file} 2> ${error_file} &
    
    count=0
    while test ! -s ${output_file} -a ! -s ${error_file} -a ${count} -lt 30
    do
	sleep 1
	count=`expr ${count} + 1`
    done

    if test -s ${error_file}
    then
	Fatal=`grep "FATAL" ${error_file}`
	if test -n "$Fatal"
	then
	    echo "FAILED"
	    cat ${error_file}
	    exit 1
	fi
    fi
    if test -s ${output_file}
    then
    	echo "Ok"
    else
	echo "FAILED"
	exit 2
    fi
    echo "----------------------------------------------------------------------"
}
trap deactivate 1 2 3 4 5 6 7 8 10 12 13 14 15



echo "##########################################################################################"
echo "Starting mirroring partition for the '$1' master at `date`				"
echo "##########################################################################################"

start_partition $1 ${port_number} ${log_dir}

host_name=`uname -n`
TDAQ_IPC_INIT_REF=`ipc_mk_ref -H ${host_name} -P ${port_number} -q`; export TDAQ_IPC_INIT_REF
TDAQ_IPC_ALLOW_MULTIPLE_DOMAINS=1; export TDAQ_IPC_ALLOW_MULTIPLE_DOMAINS

is_server_names="PMG DF DQM RunCtrl RunParams Setup Histogramming RunCtrlStatistics LumiBlock \
		LArHistogramming L1CT L1CT-History IS-TileEFMon IS-TileEFMon-Segment\
		L1CaloHistos L1TriggerRates L1CaloStatus LargParams TileParams \
		DF-L2-Segment-1-1-iss DF-L2-Segment-2-1-iss \
		DF-L2SV-Segment-1-iss DF-L2SV-Segment-2-iss \
		DF-L1Calo-iss DF-EFRack3-iss DF-EFRack4-iss "
for name in ${is_server_names} 
do
	start_is_server $1 ${name} ${log_dir}
done

rdb_server_names="RDB ISRepository"
for name in ${rdb_server_names} 
do
	start_rdb_server $1 ${name} ${log_dir}
done

start_mrs_server $1 ${log_dir}

echo "##########################################################################################"
echo "Success! `date`"
echo "Initial partition reference is:								"
echo "$TDAQ_IPC_INIT_REF									"
echo "##########################################################################################"
