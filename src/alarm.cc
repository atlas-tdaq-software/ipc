////////////////////////////////////////////////////////////////////////////
//      alarm.cc
//
//      header file for the IPC library
//
//      Sergei Kolos June 2003
//
//    description:
//	This file contains IPCAlarm class implementation
//
////////////////////////////////////////////////////////////////////////////

/*! \file alarm.cc Implements the IPCAlarm class. 
 * \author Serguei Kolos
 * \version 1.0 
 */

#include <ipc/alarm.h>

#include <boost/bind/bind.hpp>
#include <boost/bind/protect.hpp>
#include <boost/date_time/posix_time/posix_time.hpp>
#include <boost/thread/thread_time.hpp>

IPCAlarm::IPCAlarm( long seconds, long nanoseconds, const boost::function<bool ( void )> & callback )
  : m_suspended( false ),
    m_terminated( false ),
    m_thread( boost::bind(  &IPCAlarm::threadBody,
			    this,
			    callback,
			    seconds,
			    nanoseconds ) )
{ ; }

IPCAlarm::IPCAlarm( double period, const boost::function<bool ( void )> & callback )
  : m_suspended( false ),
    m_terminated( false ),
    m_thread( boost::bind(  &IPCAlarm::threadBody,
			    this,
			    callback,
			    (long)period,
			    (long)((period-(long)period)*1000000000.)) )
{ ; }

IPCAlarm::IPCAlarm( long seconds, long nanoseconds, IPCAlarm::Callback callback, void * param ) 
  : m_suspended( false ),
    m_terminated( false ),
    m_thread( boost::bind(  &IPCAlarm::threadBody,
			    this,
			    boost::protect( boost::bind( callback, param ) ),
			    seconds,
			    nanoseconds ) )
{ ; }

IPCAlarm::IPCAlarm( double period, IPCAlarm::Callback callback, void * param ) 
  : m_suspended( false ),
    m_terminated( false ),
    m_thread( boost::bind(  &IPCAlarm::threadBody,
			    this,
			    boost::protect( boost::bind( callback, param ) ),
			    (long)period,
			    (long)((period-(long)period)*1000000000.)) )
{ ; }

IPCAlarm::~IPCAlarm ( ) 
{
    m_terminated = true;
    m_thread.interrupt();
    m_thread.join();
}
    
void 
IPCAlarm::suspend() 
{
    boost::mutex::scoped_lock lock( m_mutex );
    m_suspended = true;
}

void 
IPCAlarm::resume() 
{
    boost::mutex::scoped_lock lock( m_mutex );
    m_suspended = false;
}

void 
IPCAlarm::threadBody( boost::function<bool (void)> callback, long s, long ns )
{
    while ( !m_terminated )
    {
	boost::system_time time = boost::get_system_time()
		    + boost::posix_time::seconds( s )
		    + boost::posix_time::microseconds( ns/1000 );
	try {
	    boost::thread::sleep( time );
	    boost::mutex::scoped_lock lock( m_mutex );
	    if ( !m_suspended )
		m_suspended = !callback( );
	}
	catch ( const boost::thread_interrupted & ex ) {
	    break ;
	}
    }
}
