// Copyright 2021 CERN for the benefit of the ATLAS Collaboration

//
// Define a custom sslContext and install it
// before the default omniORB version is created.
//
//     - generate new key
//     - create new certificate with key
//     - self-sign the certifate
//     - set certificate/key/CA from these
//     - set verifyMode to none
//

#include <omniORB4/CORBA.h>
#include <omniORB4/sslContext.h>

#include <openssl/ssl.h>

#include <string>
#include <stdlib.h>
#include <unistd.h>

namespace {

    class SSLContext : public sslContext {
    public:
        SSLContext();
        virtual ~SSLContext();

        virtual void set_CA() override;
        virtual void set_certificate() override;
        virtual void set_privatekey() override;

    private:
        X509     *m_cert;  // Self signed certificate
        EVP_PKEY *m_key;   // Private key
    };

    SSLContext::SSLContext()
        : sslContext(sslContext::certificate_authority_file, sslContext::key_file, sslContext::key_file_password),
          m_cert(X509_new()),
          m_key(EVP_EC_gen("P-256"))
    {
        // serial number == 1
        ASN1_INTEGER_set(X509_get_serialNumber(m_cert), 1);

        // lifetime == 356 days
        X509_gmtime_adj(X509_get_notBefore(m_cert), 0);
        X509_gmtime_adj(X509_get_notAfter(m_cert), 31536000L);

        // Set our public key
        X509_set_pubkey(m_cert, m_key);

        // Name = O=ATLAS, OU=TDAQ, CN=<PID>
        X509_NAME *name = X509_get_subject_name(m_cert);
        X509_NAME_add_entry_by_txt(name, "O",  MBSTRING_ASC,
                                   (unsigned char *)"ATLAS", -1, -1, 0);
        X509_NAME_add_entry_by_txt(name, "OU",  MBSTRING_ASC,
                                   (unsigned char *)"TDAQ", -1, -1, 0);
        std::string pid = std::to_string(getpid());
        X509_NAME_add_entry_by_txt(name, "CN", MBSTRING_ASC,
                                   (unsigned char *)pid.c_str(), -1, -1, 0);

        // Set issuer to same name
        X509_set_issuer_name(m_cert, name);

        // Sign and done
        X509_sign(m_cert, m_key, EVP_sha256());

        sslContext::verify_mode = SSL_VERIFY_NONE;
    }

    SSLContext::~SSLContext()
    {
        X509_free(m_cert);
        EVP_PKEY_free(m_key);
    }

    void
    SSLContext::set_CA()
    {
    }

    void
    SSLContext::set_certificate()
    {
        SSL_CTX_use_certificate(get_SSL_CTX(), m_cert);
    }

    void
    SSLContext::set_privatekey()
    {
        SSL_CTX_use_PrivateKey(get_SSL_CTX(), m_key);
    }

}

namespace ipc {

    void
    initialize_ssl_context()
    {
        if(const char *ssl = getenv("TDAQ_IPC_ENABLE_TLS")) {
            if(strcmp(ssl, "0") == 0) {
                return;
            }
        }
        sslContext::singleton = new SSLContext();
    }
}
