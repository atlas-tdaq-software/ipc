#include <ers/ers.h>
#include <ipc/interceptors.h>
#include <iostream>

class IPCClientTrace : public IPCClientInterceptor
{
    void sendRequest( const omniIOR & , const char * interface_id, const char * operation_id, IOP::ServiceContextList & )
    {
    	ERS_LOG( operation_id << " request sent to the object of \"" << interface_id << "\" type" );
    }
};

extern "C" void * create_ipccltrace()
{
    return new IPCClientTrace;
}
