////////////////////////////////////////////////////////////////////////////
//
//      objectadapter.cc
//
//      IPC library
//
//      Sergei Kolos June 2006
//
//	description:
//		This file contains implementations for some methods
//		of the IPCObjectAdapter class
//
////////////////////////////////////////////////////////////////////////////
#include <ipc/objectadapter.h>

std::ostream & operator<<(std::ostream & out,
	const PortableServer::ObjectId_var & oid_var)
{
    const PortableServer::ObjectId & id = oid_var.in();
    int len = id.length();

    for (int i = 0; i < len; i++) {
	out << id[i];
    }
    return out;
}

void IPCObjectAdapter::remove(PortableServer::ServantBase * servant) const
{
    PortableServer::ObjectId_var objectid;
    try {
	objectid = m_poa->servant_to_id(servant);
	m_poa->deactivate_object(objectid.in());
    }
    catch (CORBA::UserException & ex) {
	ers::debug(daq::ipc::CorbaUserException(ERS_HERE, ex._name()), 1);
        return;
    }
    catch (CORBA::SystemException & ex) {
	ers::debug(daq::ipc::CorbaSystemException(ERS_HERE, &ex), 1);
        return;
    }

    CORBA::String_var pid;
    ERS_DEBUG( 1,
	    " The servant \"" << objectid << "\" has been removed from the \"" << ( pid = m_poa->the_name() ) << "\" adapter");
}
