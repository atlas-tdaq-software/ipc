////////////////////////////////////////////////////////////////////////////
//      partition.cc
//
//      source file for the IPC library
//
//      Sergei Kolos August 2003
//
//      description:
//              This file contains implementation for IPCPartition class
////////////////////////////////////////////////////////////////////////////
#include <iostream>

#include <ipc/partition.h>
#include <ipc/util.h>


void 
IPCPartition::publish ( const std::string & object_name,
			const std::string & type_name,
                        const ipc::servant_ptr obj ) const
{
    return ipc::util::bind( m_name, object_name, type_name, obj );
}

void 
IPCPartition::withdraw ( const std::string & object_name,
			 const std::string & type_name ) const
{
    return ipc::util::unbind( m_name, object_name, type_name );
}

bool 
IPCPartition::isValid ( ) const throw()
{
    try {
    	ipc::partition_var pi = ipc::use_cache<	ipc::partition,
						ipc::non_existent >::resolve<ipc::util::resolvePartition>( m_name );
	return ( !CORBA::is_nil( pi ) );
    }
    catch ( daq::ipc::Exception & ex ) {
	return false;
    }
}

void 
IPCPartition::getTypes ( std::list< std::string > & list ) const
{
    CosNaming::BindingList_var bl;
    
    ipc::util::list ( m_name, "", bl );
    
    if ( bl.operator->() == 0 )
    	return;
    
    for ( unsigned int i = 0; i < bl->length(); i++ ) 
    {
	if ( bl[i].binding_type == CosNaming::ncontext )
        {
	    list.push_back( std::string( bl[i].binding_name[0].id ) );
        }
    }
}

ipc::partition_var 
IPCPartition::getImplementation() const
{
    ipc::partition_var pi = ipc::util::getPartition( m_name );
    if ( CORBA::is_nil(pi) )
    {
	throw daq::ipc::InvalidPartition( ERS_HERE, m_name );
    }
    return pi;
}

void 
IPCPartition::getPartitions ( std::list< IPCPartition > & list ) throw()
{
    IPCPartition::getPartitions<ipc::use_cache>( list );
}
