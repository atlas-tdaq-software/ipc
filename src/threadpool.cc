////////////////////////////////////////////////////////////////////////////
//
//     threadpool.cc
//
//     source file for IPC library
//
//     Sergei Kolos December 2007
//
//     description:
//       This file contains implementation for the IPCThreadPool class
//
////////////////////////////////////////////////////////////////////////////
#include <algorithm>
#include <iostream>

#include <boost/bind/bind.hpp>

#include <ipc/threadpool.h>

IPCThreadPool::IPCThreadPool( size_t size, const Job & thread_initializer )
  : m_job_submitter( 
  	boost::bind( size ? &IPCThreadPool::defaultJobSubmitter 
        		  : &IPCThreadPool::nullJobSubmitter, this, boost::placeholders::_1) )
{
    for ( size_t i = 0; i < size; ++i )
    {
        m_workers.push_back( WorkerPtr( new Worker( ) ) );
        m_pool.create_thread( boost::bind( &IPCThreadPool::run,
        	this, thread_initializer, m_workers.back().get() ) );
    }
}
 
IPCThreadPool::~IPCThreadPool()
{
    waitForCompletion();
    
    stopAllThreads();
    
    m_pool.join_all();
}

void 
IPCThreadPool::stopAllThreads()
{
    boost::mutex::scoped_lock lock( m_mutex );
    for ( size_t i = 0; i < m_workers.size(); ++i )
    {
	m_workers[i] -> stop();
    }
    m_jobs_available.notify_all( );
}

void 
IPCThreadPool::defaultJobSubmitter( const Job & job )
{
    boost::mutex::scoped_lock lock( m_mutex );
    m_jobs.push( job );
    m_jobs_available.notify_one( );
}

void
IPCThreadPool::addJob( const Job & job )
{
    m_job_submitter( job );
}

void
IPCThreadPool::cancel( )
{
    {
	boost::mutex::scoped_lock lock( m_mutex );
	std::queue<Job> empty;
	std::swap( m_jobs, empty );
    }
    waitForCompletion();
}

void
IPCThreadPool::waitForCompletion() const
{
    waitAllJobsDone();
    
    // make sure that all ongoing jobs have finished already
    for ( size_t i = 0; i < m_workers.size(); ++i )
    {
	boost::mutex::scoped_lock( *( m_workers[i].get() ) );
    }
}

void
IPCThreadPool::waitAllJobsDone() const
{
    boost::mutex::scoped_lock lock( m_mutex );
    m_all_jobs_done.wait( lock, boost::bind( &std::queue<Job>::empty, &m_jobs ) );
}    

void
IPCThreadPool::run( const Job & thread_initializer, Worker * worker )
{
    thread_initializer();
    
    boost::mutex::scoped_lock lock( m_mutex );

    while ( !worker -> stopped() )
    {
	m_jobs_available.wait( lock, boost::bind(
		&IPCThreadPool::Worker::awake_condition, worker, &m_jobs ) );

	if ( !m_jobs.empty() )
	{
	    boost::mutex::scoped_lock wl( *worker );

	    Job job = m_jobs.front();
	    m_jobs.pop();
            
	    if ( m_jobs.empty() )
	    {
		m_all_jobs_done.notify_one( );
	    }

	    // run the job
	    lock.unlock();
	    job();
	    lock.lock();
	}
    }
}
