#include <errno.h>

#include <sstream>
#include <fstream>

#include <ers/ers.h>

#include <ipc/util.h>
#include <ipc/core.h>
#include <ipc/policy.h>

#define IPC_REFERENCE_LOCATION_FILE	"/com/ipc_reference_location"

namespace
{

    /*! Constructs a Naming Service name
     \param	name				    Naming Service name, which will be filled by this function
     \param	object_name			    object's name (can be empty)
     \param	type_name			    object's type name (can be empty)
     \param	object_name_must_not_be_empty	    assertion is generated if this parameter is true and the
     object's name is empty
     \throw	Never throws exceptions
     */
    void makeName(CosNaming::Name & name, const std::string & object_name,
	    const std::string & type_name, bool object_name_must_not_be_empty =
		    false)
    {
	if (object_name_must_not_be_empty && object_name.empty()) {
	    throw daq::ipc::InvalidObjectName(ERS_HERE);
	}

	int size = 1;

	if ( !type_name.empty() )
	{
	    name.length( size );
	    name[size-1].id = type_name.c_str();
	    name[size-1].kind = ipc::Kind;
	    size++;
	}

	if ( !object_name.empty() )
	{
	    name.length( size );
	    name[size-1].id = object_name.c_str();
	    name[size-1].kind = ipc::Kind;
	}
    }

    std::string * get_initial_reference()
    {
	static const char * const tdaq_inst_path = getenv("TDAQ_INST_PATH");
	static const char * const initial_reference = getenv(
		"TDAQ_IPC_INIT_REF");

	///////////////////////////////////////////////////////////////////////////////////////
	// This environment is used in order to be able to run
	// multiple IPC domains for mirroring partitions
	///////////////////////////////////////////////////////////////////////////////////////
	bool multiple_domains_allowed = getenv(
		"TDAQ_IPC_ALLOW_MULTIPLE_DOMAINS") != 0;

	if (!(initial_reference && multiple_domains_allowed)
		&& tdaq_inst_path) {
	    std::string file(tdaq_inst_path);
	    file += IPC_REFERENCE_LOCATION_FILE;
	    std::ifstream fin(file.c_str());
	    if (fin && !fin.bad()) {
		ERS_DEBUG( 2,
			"reading IPC initial reference location from the '"
			<< file << "' file");
		std::string location;
		fin >> location;
		if (!fin.bad()) {
		    ERS_DEBUG( 1,
			    "IPC initial reference is 'file:" << location << "'");
		    return new std::string("file:" + location);
		}
		else {
		    ERS_DEBUG( 1,
			    "can not read IPC initial reference location from the '"
			    << file << "' file");
		}
	    }
	    else {
		ERS_DEBUG( 2,
			"IPC initial reference location file '"
			<< file << "' does not exist or not readable");
	    }
	}

	std::string * reference = new std::string(
		initial_reference ? initial_reference : IPC_DEFAULT_REFERENCE);

	ERS_DEBUG( 1, "IPC initial reference is '" << *reference << "'");
	return reference;
    }
}

/*! Resolves the initial IPC reference. This method actually parses the environment
 variable and handles the different types of reference formats
 \return the CORBA object which was constructed from the initial reference
 \throw  daq::ipc::BadReferenceFormat 		reference has unrecognized format
 \throw  daq::ipc::CannotOpenFile		can't open initial reference file
 \throw  daq::ipc::CannotReadFile		can't read initial reference file
 \throw  daq::ipc::NotInitialized    		IPC was not initialized ( i.e. IPCCore::init function was not called )
 \throw  daq::daq::ipc::CorbaSystemException	manifests CORBA internal issue
 */
CORBA::Object_ptr ipc::util::resolveInitialReference(const std::string &)
{
    std::string reference = getInitialReference();
    std::string initial_ior_;

    ///////////////////////////////////////////////////
    // first check for protocols in the reference
    // initial_ior_ will contain the actual reference
    ///////////////////////////////////////////////////
    if (reference.find("file:") == 0) {
	std::string file = reference.substr(5);
	std::ifstream fin(file.c_str());
	if (!fin || fin.bad()) {
	    throw daq::ipc::CannotOpenFile(ERS_HERE, file, strerror(errno) );
	}
	else
	{
	    fin >> initial_ior_;
	    if ( fin.bad() )
	    {
		throw daq::ipc::CannotReadFile( ERS_HERE, file, strerror(errno) );    
	    }
	}
    }
    else {
        initial_ior_ = reference;
    }

    ///////////////////////////////////////////////////
    // We have handled indirections
    // We now check the content of the reference
    ///////////////////////////////////////////////////

    if (initial_ior_.find("corbaloc:") == 0 || initial_ior_.find("IOR:") == 0) {
	return IPCCore::stringToObject(initial_ior_);
    }

    throw daq::ipc::BadReferenceFormat(ERS_HERE, initial_ior_ );
}

std::string
ipc::util::getInitialObjectReference() {
    std::string reference = getInitialReference();
    std::string initial_ior_;

    ///////////////////////////////////////////////////
    // first check for protocols in the reference
    // initial_ior_ will contain the actual reference
    ///////////////////////////////////////////////////
    if (reference.find("file:") == 0) {
        std::string file = reference.substr(5);
        std::ifstream fin(file.c_str());
        if (!fin || fin.bad()) {
            throw daq::ipc::CannotOpenFile(ERS_HERE, file, strerror(errno) );
        }
        else
        {
            fin >> initial_ior_;
            if ( fin.bad() )
            {
                throw daq::ipc::CannotReadFile( ERS_HERE, file, strerror(errno) );
            }
        }
    }
    else {
        initial_ior_ = reference;
    }

    return initial_ior_;
}

/*! Returns the current IPC initial reference
 \return the current IPC initial reference
 \throw  Never throws exceptions
 */
const std::string &
ipc::util::getInitialReference(void) throw ()
{
    //////////////////////////////////////////////////////////
    // This object is allocated with new to make sure that
    // it is never destroyed
    //////////////////////////////////////////////////////////
    static std::string * initial_reference = get_initial_reference();
    return (*initial_reference);
}

/*! Writes initial reference to a file specified by the TDAQ_IPC_INIT_REF environment variable
 \param  ior the reference string
 \throw  daq::ipc::NotFileReference	TDAQ_IPC_INIT_REF environment defines corbaloc or IOR reference which can not be used for storing the new one
 \throw  daq::ipc::BadReferenceFormat	TDAQ_IPC_INIT_REF environment does not specify a valid IPC reference
 \throw  daq::ipc::CannotOpenFile	the file specified by the TDAQ_IPC_INIT_REF environment can not be opened
 \throw  daq::ipc::CannotWriteFile	can't write to the file specified by the TDAQ_IPC_INIT_REF environment
 */
void ipc::util::storeAsInitialReference(const std::string & ior)
{
    std::string ref = getInitialReference();

    if (ref.find("corbaloc:") == 0 || ref.find("IOR:") == 0) {
	throw daq::ipc::NotFileReference(ERS_HERE, ref );
    }

    if (ref.find("file:") != 0) {
	throw daq::ipc::BadReferenceFormat(ERS_HERE, ref );
    }

    std::string file = ref.substr(5);
    std::ofstream fout(file.c_str(), std::ios::out | std::ios::trunc);
    if (!fout) {
	throw daq::ipc::CannotOpenFile(ERS_HERE, file, strerror(errno) );
    }

    fout << ior;
    fout.close();

    if (fout.fail() || fout.bad()) {
	throw daq::ipc::CannotWriteFile(ERS_HERE, file, strerror(errno) );
    }
}

/*! Builds a string that contains a Corba object reference in the  url /corbaloc format
 \param  partition_name IPC partition name
 \param  object_name 	CORBA object name
 \param  type_name	CORBA object type name
 \param  endpoints 	list of endpoints where each endpoint is represented
 by a pair of either <ip number,tcp port number> or <host name,tcp port number>
 \return CORBA object reference
 \throw  Never throws exceptions
 */
std::string ipc::util::constructReference(const std::string & partition_name,
	const std::string & object_name,
	const std::string & type_name,
        const std::vector<std::pair<std::string, int> > & endpoints,
        bool tls) throw ()
{
    std::ostringstream out;
    out << "corbaloc:";

    for (size_t i = 0; i < endpoints.size(); i++) {
      out << ( tls ? "ssliop:" : "iiop:" ) << endpoints[i].first << ":" << endpoints[i].second;
      if ((i + 1) < endpoints.size())
        out << ",";
    }
    out << "/%ff" << type_name << "%00" << partition_name << "/" << type_name << "/" << object_name;
    return out.str();
}

/*! Builds a string that contains a Corba object reference in the  url /corbaloc format
 \param  object_name 	CORBA object name
 \param  type_name	CORBA object type name
 \param  endpoints 	list of endpoints where each endpoint is represented
 by a pair of either <ip number,tcp port number> or <host name,tcp port number>
 \return CORBA object reference
 \throw  Never throws exceptions
 */
std::string ipc::util::constructReference(const std::string & object_name,
	const std::string & type_name,
	const std::vector<std::pair<std::string, int> > & endpoints) throw ()
{
    std::ostringstream out;
    out << "corbaloc:";

    for (size_t i = 0; i < endpoints.size(); i++) {
	out << "iiop:" << endpoints[i].first << ":" << endpoints[i].second;
	if ((i + 1) < endpoints.size())
	    out << ",";
    }
    out << "/%ff" << type_name << "%00" << object_name;
    return out.str();
}

/*! Builds a string that contains a Corba object reference in the  url /corbaloc format 
 \param  object_name 	CORBA object name
 \param  type_name	CORBA object type name
 \param  file_name 	a file name to be used by the Unix domain socket
 \return CORBA object reference
 \throw  Never throws exceptions
 */
std::string ipc::util::constructReference(const std::string & object_name,
	const std::string & type_name, const std::string & file_name) throw ()
{
    std::ostringstream out;
    out << "corbaloc:omniunix:" << file_name << ":/%ff" << type_name << "%00"
	    << object_name;
    return out.str();
}

ipc::partition_ptr ipc::util::getPartition(const std::string & partition_name)
{
    try {
	if (partition_name == partition::default_name) {
	    return use_cache<partition, narrow>::resolve<resolveInitialReference>(
		    partition_name);
	}
	else {
	    return use_cache<partition, narrow>::resolve<resolvePartition>(
		    partition_name);
	}
    }
    catch (daq::ipc::Exception & ex) {
	throw daq::ipc::InvalidPartition(ERS_HERE, partition_name, ex );
    }
}

	/*! Looks up a reference to the IPC partition object
	 \param  partition_name		partition name
	 \return CORBA reference to the target partition
	 \throw  daq::ipc::InvalidPartition	partition IPC server is unavailable, i.e. either it is not
	 running or there are problems with network connection
	 */
CORBA::Object_ptr ipc::util::resolvePartition(
	const std::string & partition_name)
{
    ipc::partition_var ip;

    try {
	if (partition_name == partition::default_name) {
	    ip = no_cache<partition, narrow>::resolve<resolveInitialReference>(
		    partition::default_name);
	}
	else {
	    ip = use_cache<partition, narrow>::resolve<resolveInitialReference>(
		    partition::default_name);
	}
    }
    catch (daq::ipc::Exception & ex) {
	throw daq::ipc::InvalidPartition(ERS_HERE, partition_name, ex );
    }

    if (CORBA::is_nil(ip)) {
	throw daq::ipc::InvalidPartition(ERS_HERE, partition::default_name );
    }

    if (partition_name == partition::default_name) {
	return CORBA::Object::_narrow(ip);
    }

    try {
	CosNaming::NamingContext_var nc = findContext(ip, partition_name);

	return CORBA::Object::_narrow(nc);
    }
    catch (CORBA::SystemException & ex) {
	throw daq::ipc::InvalidPartition(ERS_HERE, partition_name,
		daq::ipc::CorbaSystemException( ERS_HERE, &ex ) );
    }
    catch(CORBA::UserException & ex) {
	throw daq::ipc::InvalidPartition( ERS_HERE, partition_name,
		daq::ipc::CorbaUserException( ERS_HERE, ex._name() ) );
    }
}

/*! Looks up an object in a partition by name and type
 \param  partition_name			partition name
 \param  object_name			object name
 \param  type_name			object type
 \return 				CORBA reference to the target object
 \throw  daq::ipc::InvalidPartition	partition IPC server is unavailable, i.e. either it is not
 running or there are problems with network connection
 \throw  daq::ipc::ObjectNotFound	target object does not exist or is not registered with the partition server.
 \throw  daq::ipc::InvalidObjectName	object's name is invalid.
 */
CORBA::Object_ptr ipc::util::resolve(const std::string & partition_name,
	const std::string & object_name, const std::string & type_name)
{
    CosNaming::Name cns;

    makeName(cns, object_name, type_name, true);

    ipc::partition_var ip = getPartition(partition_name);

    if (CORBA::is_nil(ip)) {
	throw daq::ipc::InvalidPartition(ERS_HERE, partition_name );
    }

    CORBA::Object_var object;
    try {
	ipc::URI_var uri;
	ip->obtain(cns, uri);
	object = IPCCore::getORB()->string_to_object(
		(const char *) uri->get_buffer());
	if (CORBA::is_nil(object)) {
	    throw daq::ipc::ObjectNotFound(ERS_HERE, partition_name,
		    type_name,object_name );
	}
    }
    catch(CosNaming::NamingContext::NotFound& ex)
    {
	throw daq::ipc::ObjectNotFound( ERS_HERE, partition_name,
		type_name, object_name );
    }
    catch(CORBA::SystemException & ex)
    {
	throw daq::ipc::InvalidPartition( ERS_HERE, partition_name,
		daq::ipc::CorbaSystemException( ERS_HERE, &ex ) );
    }
    catch(CORBA::UserException & ex)
    {
	throw daq::ipc::InvalidPartition( ERS_HERE, partition_name,
		daq::ipc::CorbaUserException( ERS_HERE, ex._name() ) );
    }
    return CORBA::Object::_duplicate(object);
}

/*! Builds a list of objects which are registered with the given IPC partition
 \param  partition_name 		partition name
 \param  type_name 			type of the object's which have to be listed
 \param  bl 				this list will be filled with the matched objects
 \throw  daq::ipc::InvalidPartition	partition IPC server is unavailable, i.e. either it is not
 running or there are problems with network connection

 */
void ipc::util::list(const std::string & partition_name,
	const std::string & type_name, CosNaming::BindingList_var & bl)
{
    CosNaming::BindingIterator_var bi;

    ipc::partition_var ip = getPartition(partition_name);

    if (CORBA::is_nil(ip)) {
	throw daq::ipc::InvalidPartition(ERS_HERE, partition_name );
    }

    try {
	CosNaming::NamingContext_var nc;
	if (type_name.empty()) {
	    /////////////////////////////////////////////////////////////////
	    // List of partitions is requested.
	    // Partitions are always registered with the Root context.
	    /////////////////////////////////////////////////////////////////
	    nc = CosNaming::NamingContext::_duplicate(ip);
	}
	else {
	    nc = findContext(ip, type_name);
	}

	if (!CORBA::is_nil(nc)) {
	    nc->list(100000, bl, bi);
	}
    }
    catch (CosNaming::NamingContext::NotFound & ex) {
	ERS_DEBUG( 1, "The '" << type_name << "' context does not exist");
	// context does not exist - the list will be empty
    }
    catch (CORBA::SystemException & ex) {
	throw daq::ipc::InvalidPartition(ERS_HERE, partition_name,
		daq::ipc::CorbaSystemException( ERS_HERE, &ex ) );
    }
    catch(CORBA::UserException & ex) {
	throw daq::ipc::InvalidPartition( ERS_HERE, partition_name,
		daq::ipc::CorbaUserException( ERS_HERE, ex._name() ) );
    }
}

/*! Registers new object with the IPC partition
 \param  partition_name		name of the partition where the object is registered
 \param  object_name 		name of the object
 \param  type_name			type of the object
 \param  obj				CORBA reference to object which has to be registered
 \throw  daq::ipc::InvalidPartition	partition IPC server is unavailable, i.e. either it is not
 running or there are problems with network connection
 \throw  daq::ipc::InvalidObjectName	object's name is invalid.
 */
void ipc::util::bind(const std::string & partition_name,
	const std::string & object_name, const std::string & type_name,
	CORBA::Object_ptr obj)
{

    ERS_DEBUG( 1, "binding object '" << object_name << "' of the '"
	    << type_name << "' in the '" << partition_name << "' partition");

    CosNaming::Name cns;

    makeName(cns, object_name, type_name, true);

    ipc::partition_var ip = getPartition(partition_name);

    if (CORBA::is_nil(ip)) {
	throw daq::ipc::InvalidPartition(ERS_HERE, partition_name );
    }

    while (1) {
	try {
	    char * s = IPCCore::getORB()->object_to_string(obj);
	    CORBA::ULong l = strlen(s) + 1;
	    ipc::URI uri(l, l, (CORBA::Octet*) s, true);
	    ip->associate(cns, uri);
	    break;
	}
	catch (CosNaming::NamingContext::NotFound &) {
	    try {
		createContext(ip, type_name);
		continue;
	    }
	    catch (CosNaming::NamingContext::AlreadyBound & ex) {
		ERS_DEBUG( 1,
			"Context '" << type_name << "' has been bound already");
		continue;
	    }
	    catch (CORBA::SystemException & ex) {
		throw daq::ipc::InvalidPartition(ERS_HERE, partition_name,
			daq::ipc::CorbaSystemException( ERS_HERE, &ex ) );
	    }
	}
	catch(CORBA::SystemException & ex)
	{
	    throw daq::ipc::InvalidPartition( ERS_HERE, partition_name,
		    daq::ipc::CorbaSystemException( ERS_HERE, &ex ) );
	}
	catch(CORBA::UserException & ex)
	{
	    throw daq::ipc::InvalidPartition( ERS_HERE, partition_name,
		    daq::ipc::CorbaUserException( ERS_HERE, ex._name() ) );
	}
    }
}

/*! Removes an object binding from the IPC partition server 
 \param  partition_name 		name of the partition server (aka partition name)
 \param  object_name			name of the object
 \param  type_name			object's type
 \throw  daq::ipc::ObjectNotFound 	the object is not registered with the given partition
 \throw  daq::ipc::InvalidPartition 	partition IPC server is unavailable, i.e. either it is not
 running or there are problems with network connection
 \throw  daq::ipc::InvalidObjectName	object's name is invalid.
 */
void ipc::util::unbind(const std::string & partition_name,
	const std::string & object_name, const std::string & type_name)
{

    CosNaming::Name cns;

    makeName(cns, object_name, type_name, true);

    ipc::partition_var ip = getPartition(partition_name);

    if (CORBA::is_nil(ip)) {
	throw daq::ipc::InvalidPartition(ERS_HERE, partition_name );
    }

    try {
	ip->unbind(cns);
    }
    catch (CosNaming::NamingContext::NotFound& ex) {
	throw daq::ipc::ObjectNotFound(ERS_HERE, partition_name, type_name,object_name );
    }
    catch(CORBA::SystemException & ex) {
	throw daq::ipc::InvalidPartition( ERS_HERE, partition_name,
		daq::ipc::CorbaSystemException( ERS_HERE, &ex ) );
    }
    catch(CORBA::UserException & ex)
    {
	throw daq::ipc::InvalidPartition( ERS_HERE, partition_name,
		daq::ipc::CorbaUserException( ERS_HERE, ex._name() ) );
    }
}

/*! Creates and binds new naming context 
 \param  ip partition in which the new context will be created
 \param  name name of the new context
 \throw  CosNaming::NamingContext::AlreadyBound
 \throw  CORBA::SystemException
 */
void ipc::util::createContext(ipc::partition_ptr ip, const std::string & name)
{
    ERS_ASSERT( !CORBA::is_nil(ip));

    CosNaming::Name cns;
    makeName(cns, "", name);
    CosNaming::NamingContext_var contextHolder = ip->bind_new_context(cns);
}

/*! Finds the Naming Context in the appropriate IPC partition
 \param  ip 					reference to the IPC partition
 \param  name 					Naming Context name
 \return 					the pointer to the Naming Context
 \throw  CosNaming::NamingContext::NotFound 	the context does not exist
 \throw  CORBA::SystemException 		communication with the IPC partition 
 failed because it is not running or there are problems with network connection
 */
CosNaming::NamingContext_ptr ipc::util::findContext(ipc::partition_ptr ip,
	const std::string & name)
{
    IPC_ASSERT( !CORBA::is_nil(ip))

    CosNaming::Name cns;
    makeName(cns, "", name);
    CosNaming::NamingContext_var nc;

    CORBA::Object_var object = ip->resolve(cns);
    nc = CosNaming::NamingContext::_narrow(object);

    if (CORBA::is_nil(nc)) {
	throw CosNaming::NamingContext::NotFound();
    }

    return CosNaming::NamingContext::_duplicate(nc);
}

/*! Extracts object's type id from the internal CORBA object id
 \param  object_name full CORBA object id
 \return string representing object's type
 \throw  Never throws exceptions
 */
std::string ipc::util::extractTypeName(const std::string & object_name) throw ()
{
    std::string name = object_name;
    std::string::size_type pos = name.find(':');

    IPC_ASSERT( pos != std::string::npos)
    name.erase(0, pos + 1);
    pos = name.find(':');

    IPC_ASSERT( pos != std::string::npos)
    name.erase(pos);

    return name;
}

/*! Extracts object's type name from the CORBA servant
 \param  servant type name for this CORBA servant will be extracted
 \return type name for the given servant
 \throw  Never throws exceptions
 */
std::string ipc::util::getTypeName(
	const PortableServer::ServantBase * servant) throw ()
{
    IPC_ASSERT( servant)
    return extractTypeName(
	    const_cast<PortableServer::ServantBase *>(servant)->_mostDerivedRepoId());
}

bool ipc::util::isValidChar(int c)
{
    return (isalnum(c) || c == ';' || c == '/' || c == ':' || c == '?'
	    || c == ':' || c == '@' || c == '&' || c == '=' || c == '+'
	    || c == '$' || c == ',' || c == '-' || c == '_' || c == '!'
	    || c == '~' || c == '*' || c == '\'' || c == '(' || c == ')');
}

