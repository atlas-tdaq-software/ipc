#!/bin/sh
######################################################################
#
#	Script for IPC package - start all 
#	partitions necessary for the tests
#	Created by Sergei Kolos; 10.02.99
#
######################################################################

test_script_result()
{
  if test $? -ne 0
  then
	echo "*************** Check target failed ***************"
	ipc_rm -i ".*" -n ".*" -v -f
	exit 1
  fi
}

start_server()
{
	rm -f server.ref
	$@ > server.ref&
	count=0
	while test ! -s server.ref -a $count -lt 80
	do
		sleep 1
		count=`expr $count + 1`
	done
	if test ! -s server.ref
	then
		echo "Failed!"
		echo "(Server was not started)"
		exit
	else
		echo "OK!"
	fi
}
trap deactivate 1 2 3 4 5 6 7 8 10 12 13 14 15


##############################################
# Start IPC Proxy
##############################################
if test -n "$4"
then
	echo "*************** Starting IPC Proxy server ***************"
	start_server $1ipc_proxy
	test_script_result
fi

start_partition_command=`echo $0 | sed -e 's/\/run.sh/\/start_partition.sh/g'`
$start_partition_command
$start_partition_command Partition1
$start_partition_command Partition2

##############################################
# Start IPC Test
##############################################

client="$1ipc_test_client -e -N 10 -i 1000"
server="$1ipc_test_server -e -N 10 "

echo Client command is \"$client\"
echo Server command is \"$server\"

echo
echo "Waiting for the server to start up ... "
start_server $server
test_script_result
sleep 10
echo

echo
echo "Starting client..."
$client
test_script_result
echo

if test '$2'
then
	echo
	echo "Waiting for the server to start up ... "
	start_server $server
	test_script_result
	sleep 10
	echo

	if test $TDAQ_IPC_INIT_REF
	then
		ref_param="-Dtdaq.ipc.init.ref=$TDAQ_IPC_INIT_REF"
	fi
	echo "java test command is {$2 -classpath $3 $ref_param Test ipc_test_object_0_st test_ipc_object_java}"
	$2 -classpath $3 $ref_param Test ipc_test_object_0_st test_ipc_object_java
	test_script_result
	echo
fi

echo "*************** Check target finished successfuly ***************"
echo "Stopping servers ... "
ipc_rm -i ".*" -n ".*" -f
echo "done."

if test -n "$4"
then
	echo "*************** Stopping IPC Proxy server ***************"
	killall ipc_proxy
fi
