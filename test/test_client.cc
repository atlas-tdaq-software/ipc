//
//      test-client.cc
//
//      test application for IPC library
//
//      Sergei Kolos January 2000
//
//      description:
//              Shows how to access remote objects and call their methods
//	    Performs timing for the lookup and remote methods invocations
/////////////////////////////////////////////////////////////////////////////////
#include <stdio.h>

#include <ers/ers.h>

#include <ipc/partition.h>
#include <ipc/object.h>
#include <ipc/alarm.h>
#include <ipc/core.h>

#include <owl/timer.h>
#include <cmdl/cmdargs.h>
#include <unistd.h>

#include "test/test.hh"

using namespace std;

CmdArgBool check('c', "check",	"use validation check for object reference.");
CmdArgInt  delay('d', "delay",	"delay", "delay between remote requests (0 by default).");
CmdArgInt  num	('N', "Number",	"objects-number", "number of objects for timing test (100 by default).");
CmdArgInt  iter	('i', "iterations", "iterations-number", "number of iterations for timing test (1000 by default).");
CmdArgBool synch('s', "synchronous", "test just synchronous method.");
CmdArgBool asynch('a', "asynchronous", "test just asynchronous method.");
CmdArgBool echo	('e', "echo",	"output results to stdout.");
CmdArgStr  object_name('o', "object", "object-name", "object to use for the invocations test.");

int s_error_num = 0;
int a_error_num = 0;

void s_error(CORBA::Exception & ex)
{
    s_error_num++;
    std::cerr << "ERROR [synch_ping] exception " << ex._name() << std::endl;
}

void a_error(CORBA::Exception & ex)
{
    a_error_num++;
    std::cerr << "ERROR [asynch_ping] exception " << ex._name() << std::endl;
}


void TestIterators( IPCPartition p )
{
    std::map< std::string, ipc::server_var >	objects;
    
    OWLTimer time;

    time.start();
    try {
        p.getObjects<ipc::server>( objects );
    }
    catch( daq::ipc::InvalidPartition & ex ) {
    	ers::error( ex );
    }
    time.stop();
    
    if ( echo )
    {
  	std::cout << "There are " << objects.size() << " objects of type \"ipc::server\" registered with the \""
	      << p.name() << "\" partition" << std::endl;
	std::cout << "getObjects operation took (in seconds): " << std::endl
	      << "\t" << time.userTime() << "(user time)" << std::endl
	      << "\t" << time.systemTime() << "(system time)" << std::endl
	      << "\t" << time.totalTime() << "(total time)" << std::endl;
    }
    
    std::map< std::string, ipc::server_var >::iterator it;
    
    if ( echo ) 
    	std::cout << "Objects are:" << std::endl;
    for ( it = objects.begin(); it != objects.end(); it++ )
    {
  	if ( echo )
	    std::cout << "\"" << (*it).first << "\"" << std::endl;
    }	
}

void TestLookupMethod( IPCPartition p )
{
    char	oname[32];
    int		i;
    OWLTimer	time;
  
    time.start();
    for ( i = 0; i < num ; i++ )
    {
  	sprintf( oname, "ipc_test_object_%d_mt", i );
  	try {
	    ipc::server_var handle = p.lookup<ipc::server>( oname );
  	}
  	catch( daq::ipc::InvalidPartition & ex ) {
  	    ers::error( ex );
  	}
  	catch( daq::ipc::ObjectNotFound & ex ) {
  	    ers::error( ex );
  	}
	catch( daq::ipc::InvalidObjectName & ex ) {
	    ers::error( ex );
	}
    }
    time.stop();
    if ( echo )
    {
	std::cout	<< "object resolution (first time) takes (in seconds): " << std::endl
	    << "\t" << time.userTime()/num << "(user time)" << std::endl
	    << "\t" << time.systemTime()/num << "(system time)" << std::endl
	    << "\t" << time.totalTime()/num << "(total time)" << std::endl;   
    }
    
    time.reset();
    time.start();
    for ( i = 0; i < num ; i++ )
    {
  	sprintf( oname, "ipc_test_object_%d_st", i );
  	try {
	    ipc::server_var handle = p.lookup<ipc::server>( oname );
  	}
  	catch( daq::ipc::InvalidPartition & ex ) {
  	    ers::error( ex );
  	}
  	catch( daq::ipc::ObjectNotFound & ex ) {
  	    ers::error( ex );
  	}
	catch( daq::ipc::InvalidObjectName & ex ) {
	    ers::error( ex );
	}
    }
    time.stop();
    if ( echo )
    {
	std::cout	<< "object resolution (second time) takes (in seconds): " << std::endl
	    << "\t" << time.userTime()/num << "(user time)" << std::endl
	    << "\t" << time.systemTime()/num << "(system time)" << std::endl
	    << "\t" << time.totalTime()/num << "(total time)" << std::endl;   
    }
}

void TestResolve( IPCPartition p )
{
    char	oname[32] = "ipc_test_object_0_mt";
    int		i;
    OWLTimer	time;
  
    time.start();
    for ( i = 0; i < iter; i++ )
    {
  	try {
	    ipc::server_var handle = p.lookup<ipc::server,ipc::no_cache,ipc::narrow>( oname );
  	}
  	catch( daq::ipc::InvalidPartition & ex ) {
  	    ers::error( ex );
  	}
  	catch( daq::ipc::ObjectNotFound & ex ) {
  	    ers::error( ex );
  	}
	catch( daq::ipc::InvalidObjectName & ex ) {
	    ers::error( ex );
	}
    }
    time.stop();
    if ( echo )
    {
	std::cout	<< "object resolution takes (in seconds): " << std::endl
	    << "\t" << time.userTime()/iter << "(user time)" << std::endl
	    << "\t" << time.systemTime()/iter << "(system time)" << std::endl
	    << "\t" << time.totalTime()/iter << "(total time)" << std::endl;   
    }
}

void TestTestable( IPCPartition p )
{
    char	oname[32] = "testable object 1";
    int		i;
    OWLTimer	time;
  
    time.start();
    for ( i = 0; i < iter; i++ )
    {
  	try {
	    ipc::servant_var handle = p.lookup<ipc::servant,ipc::no_cache,ipc::narrow>( oname );
            handle->test("dummy");
  	}
  	catch( daq::ipc::InvalidPartition & ex ) {
  	    ers::error( ex );
  	}
  	catch( daq::ipc::ObjectNotFound & ex ) {
  	    ers::error( ex );
  	}
	catch( daq::ipc::InvalidObjectName & ex ) {
	    ers::error( ex );
	}
    }
    time.stop();
    if ( echo )
    {
	std::cout	<< "object resolution takes (in seconds): " << std::endl
	    << "\t" << time.userTime()/iter << "(user time)" << std::endl
	    << "\t" << time.systemTime()/iter << "(system time)" << std::endl
	    << "\t" << time.totalTime()/iter << "(total time)" << std::endl;   
    }
}

void TestIsObjectValid( IPCPartition p )
{
    char	oname[32];
    int		i;
 
    for ( i = 0; i < iter ; i++ )
    {
  	if ( echo )
	    std::cout << " p.isValid( ) = " << p.isValid( ) << std::endl;
    }
    
    sprintf( oname, "ipc_test_object_0_st" );
    for ( i = 0; i < iter ; i++ )
    {
	try {
	    bool v = p.isObjectValid<ipc::server>( oname );
	    if ( echo )
		std::cout << " p.isObjectValid( ) = " << v << std::endl;
	}
	catch( daq::ipc::InvalidPartition & ex ) {
	    ers::error( ex );
	}
    }
    
    for ( i = 0; i < num ; i++ )
    {
  	sprintf( oname, "ipc_test_object_%d_mt", i );
	try {
	    bool v = p.isObjectValid<ipc::server>( oname );
	    if ( echo )
		std::cout << " p.isObjectValid( ) = " << v << std::endl;
	}
	catch( daq::ipc::InvalidPartition & ex ) {
	    ers::error( ex );
	}
    }
    
}

void TestRemoteInvocation( IPCPartition p )
{
    char line[] = { 55, 0 };
    	
    OWLTimer time;
    if ( !synch )
    {
  	if ( echo )
	{
	    std::cout << "Asynchronous method test ..." << std::endl;
	}
	
	time.start();

  	for ( int i = 0; i < iter; i++ )
	{
	    sleep( delay );
	    ipc::server_var handle;
	    try {
	    	std::clog << "calling lookup ... {" << std::endl;
		handle = p.lookup<ipc::server>( (const char*)object_name );
	    	std::clog << "} done" << std::endl;
	    }
	    catch( daq::ipc::InvalidPartition & ex ) {
		ers::error( ex );
                continue;
	    }
	    catch( daq::ipc::ObjectNotFound & ex ) {
		ers::error( ex );
                continue;
	    }
	    catch( daq::ipc::InvalidObjectName & ex ) {
		ers::error( ex );
                continue;
	    }
	    
	    std::clog << "calling ping_a function ... {" << std::endl;
            try {
                handle -> ping_a( line );
	    }
	    catch(CORBA::Exception & ex) {
	    	a_error( ex );
	    }
	    std::clog << "} done" << std::endl;
  	}
	time.stop();

  	if ( echo )
	{
	    std::cout << "passing 1024 bytes (asynchronously) takes (in seconds): " << std::endl
	    	 << "\t" << time.userTime()/iter << "(user time)" << std::endl
	    	 << "\t" << time.systemTime()/iter << "(system time)" << std::endl
	    	 << "\t" << time.totalTime()/iter << "(total time)" << std::endl;   
	}
    }
  
    if ( !asynch )
    {
  	if ( echo )
	{	
	    std::cout << "Synchronous method test ..." << std::endl;
	}
  
	time.reset();
	time.start();               // Record starting time
	
   	for ( int i = 0; i < iter; i++ )
	{
	    sleep( delay );
	    ipc::server_var handle;

	    try {
	    	std::clog << "calling lookup ... {" << std::endl;
		handle = p.lookup<ipc::server>( (const char*)object_name );
	    	std::clog << "} done" << std::endl;
	    }
	    catch( daq::ipc::InvalidPartition & ex ) {
		ers::error( ex );
                continue;
	    }
	    catch( daq::ipc::ObjectNotFound & ex ) {
		ers::error( ex );
                continue;
	    }
	    catch( daq::ipc::InvalidObjectName & ex ) {
		ers::error( ex );
                continue;
	    }

	    std::clog << "calling ping_s function ... {" << std::endl;
	    try {
	    	CORBA::String_var ret = handle -> ping_s( line );
	    }
	    catch(CORBA::Exception & ex) {
	    	s_error( ex );
	    }
	    std::clog << "} done" << std::endl;
  	}
	time.stop();
  	if ( echo )
	{
	    std::cout << "passing 1024 bytes (synchronously) takes (in seconds): " << std::endl
	    	 << "\t" << time.userTime()/iter << "(user time)" << std::endl
	    	 << "\t" << time.systemTime()/iter << "(system time)" << std::endl
	    	 << "\t" << time.totalTime()/iter << "(total time)" << std::endl;   
	}
    }  
}

void TestExceptions( IPCPartition p )
{
    char    line[1024];
    	
    memset( line, 55, 1024 );
    line[1023] = 0;

    for ( int i = 0; i < iter; i++ )
    {
	ipc::server_var handle;
	try {
	    handle = p.lookup<ipc::server>( (const char*)object_name );
	}
	catch( daq::ipc::InvalidPartition & ex ) {
	    ers::error( ex );
	    break;
	}
	catch( daq::ipc::ObjectNotFound & ex ) {
	    ers::error( ex );
	    break;
	}
	catch( daq::ipc::InvalidObjectName & ex ) {
	    ers::error( ex );
	    break;
	}
        
	try
	{
	    CORBA::String_var ret = handle -> ping_s( line );
	}
	catch(CORBA::SystemException & ex)
	{
	    std::cerr << std::endl << "ERROR [ipc_test_client::TestExceptions] system exception " << ex._name() << std::endl;
	}
    }
}

class IPCCallback : public IPCObject<POA_ipc::callback>
{
  public:
    void notify( const char * name )
    {
    	std::cout << "Callback from employee \"" << name << "\" received" << std::endl;
    }
    
    ~IPCCallback()
    {
    	std::cout << "IPCCallback destructor is called" << std::endl;
    }
};

void TestCallbacks( IPCPartition p )
{
    IPCCallback * cb = new IPCCallback;
    
    std::map< std::string, ipc::person_var >	persons;
    try {
        p.getObjects<ipc::person>( persons );
    }
    catch( daq::ipc::InvalidPartition & ex ) {
    	ers::error( ex );
    }

    std::map< std::string, ipc::person_var >::iterator pit;
    
    std::cout << "Persons are:" << std::endl;
    
    for ( pit = persons.begin(); pit != persons.end(); pit++ )
    {
	std::cout << "\"" << (*pit).first << "\"" << std::endl;
    }	
    
    std::map< std::string, ipc::employee_var >	employees;
    try {
        p.getObjects<ipc::employee>( employees );
    }
    catch( daq::ipc::InvalidPartition & ex ) {
    	ers::error( ex );
    }

    std::map< std::string, ipc::employee_var >::iterator eit;
    
    std::cout << "Employees are:" << std::endl;
    
    for ( eit = employees.begin(); eit != employees.end(); eit++ )
    {
	std::cout << "\"" << (*eit).first << "\"" << std::endl;
	(*eit).second->subscribe( cb->_this(), 1 );
    }	

    ::sleep(10);
    
    cb->_destroy();
    std::cout << "Callbacks test completed." << std::endl;
}

void TestCache( IPCPartition p )
{
    if ( echo )
    {
	std::cout << "Cache test ..." << std::endl;
    }

    ipc::server_var handle1;
    ipc::server_var handle2;
    try {
	std::clog << "calling lookup for handle 1 ... {" << std::endl;
	handle1 = p.lookup<ipc::server>( (const char*)object_name );
	std::clog << "} done" << std::endl;
        
	std::clog << "calling lookup for handle 2 ... {" << std::endl;
	handle2 = p.lookup<ipc::server>( (const char*)object_name );
	std::clog << "} done" << std::endl;
    }
    catch( daq::ipc::Exception & ex ) {
	ers::error( ex );
        return;
    }

    char line[] = { 55, 0 };
    for ( int i = 0; i < iter; i++ )
    {
	try
	{
	    sleep( delay );
	    std::clog << "calling ping_s function using handle 1 ... {" << std::endl;
	    CORBA::String_var ret1 = handle1 -> ping_s( line );
	    std::clog << "} done" << std::endl;
	    
            sleep( delay );
            std::clog << "calling ping_s function using handle 2 ... {" << std::endl;
	    CORBA::String_var ret2 = handle2 -> ping_s( line );
	    std::clog << "} done" << std::endl;
	}
	catch(CORBA::Exception & ex)
	{
	    s_error( ex );
	}
            
	try
	{
            sleep( delay );
	    std::clog << "calling lookup for local handle ... {" << std::endl;
	    ipc::server_var handle = p.lookup<ipc::server>( (const char*)object_name );
	    std::clog << "} done" << std::endl;

            sleep( delay );
            std::clog << "calling ping_s function using local handle ... {" << std::endl;
	    CORBA::String_var ret = handle -> ping_s( line );
	    std::clog << "} done" << std::endl;            
	}
	catch(CORBA::Exception & ex)
	{
	    s_error( ex );
	}
	catch( daq::ipc::Exception & ex ) {
	    ers::error( ex );
	    return;
	}
    }
    
    std::cout << "Cache test completed." << std::endl;
}

void TestPerformance( IPCPartition p, size_t size )
{
    char * line = new char[size];
    	
    memset( line, 55, size );
    line[size-1] = 0;

    double total_time = 0;
    double user_time = 0;
    double system_time = 0;
    if ( !synch )
    {
  	if ( echo )
	{
	    std::cout << "Asynchronous method test ..." << std::endl;
	}
	
  	for ( int i = 0; i < iter; i++ )
	{
	    if ( delay) usleep( delay );
	    ipc::server_var handle;
	    try {
		handle = p.lookup<ipc::server>( (const char*)object_name );
	    }
	    catch( daq::ipc::InvalidPartition & ex ) {
		ers::error( ex );
                continue;
	    }
	    catch( daq::ipc::ObjectNotFound & ex ) {
		ers::error( ex );
                continue;
	    }
	    catch( daq::ipc::InvalidObjectName & ex ) {
		ers::error( ex );
                continue;
	    }
	    
            try
	    {
		OWLTimer one_time;
		one_time.start();
                handle -> ping_a( line );
		one_time.stop();
		std::cout << one_time.totalTime()*1000 << std::endl;
                total_time += one_time.totalTime();
                user_time += one_time.userTime();
                system_time += one_time.systemTime();
	    }
	    catch(CORBA::Exception & ex)
	    {
	    	a_error( ex );
	    }
  	}

  	if ( echo )
	{
	    std::cout << "passing " << size << " bytes (asynchronously) takes (in milliseconds): " << std::endl
	    	 << "\t" << user_time/iter*1000 << "(user time)" << std::endl
	    	 << "\t" << system_time/iter*1000 << "(system time)" << std::endl
	    	 << "\t" << total_time/iter*1000 << "(total time)" << std::endl;   
	}
    }
  
    total_time = 0;
    user_time = 0;
    system_time = 0;
    if ( !asynch )
    {
  	if ( echo )
	{	
	    std::cout << "Synchronous method test ..." << std::endl;
	}
  
   	for ( int i = 0; i < iter; i++ )
	{
	    if ( delay) usleep( delay );
	    ipc::server_var handle;
	    try {
		handle = p.lookup<ipc::server>( (const char*)object_name );
	    }
	    catch( daq::ipc::InvalidPartition & ex ) {
		ers::error( ex );
                continue;
	    }
	    catch( daq::ipc::ObjectNotFound & ex ) {
		ers::error( ex );
                continue;
	    }
	    catch( daq::ipc::InvalidObjectName & ex ) {
		ers::error( ex );
                continue;
	    }

	    try
	    {
		OWLTimer one_time;
		one_time.start();
	    	CORBA::String_var ret = handle -> ping_s( line );
		one_time.stop();
		std::cout << one_time.totalTime()*1000 << std::endl;
                total_time += one_time.totalTime();
                user_time += one_time.userTime();
                system_time += one_time.systemTime();
	    }
	    catch(CORBA::Exception & ex)
	    {
	    	s_error( ex );
	    }
  	}

  	if ( echo )
	{
	    std::cout << "passing " << size << " bytes (asynchronously) takes (in milliseconds): " << std::endl
	    	 << "\t" << user_time/iter*1000 << "(user time)" << std::endl
	    	 << "\t" << system_time/iter*1000 << "(system time)" << std::endl
	    	 << "\t" << total_time/iter*1000 << "(total time)" << std::endl;   
	}
    }
    
    delete[] line;
}

void TestAMI( IPCPartition p )
{
    if ( echo )
    {
        std::cout << "AMI test ..." << std::endl;
    }

    try {
        ipc::server_var handle = p.lookup<ipc::server>( (const char*)object_name );
        ipc::AMI_serverPoller_var poller = handle -> sendp_ping_s( "Test string for AMI" );

        // Poll using is_ready
        while (!poller->is_ready(100)) {
            std::cout << "Not ready" << std::endl;
        }

        CORBA::String_var result;
        poller->ping_s(0, result.out());

        std::cout << "The call returned: " << (const char*)result << std::endl;
    }
    catch(CORBA::Exception & ex)
    {
        s_error( ex );
    }

    if ( echo )
    {
        std::cout << "done" << std::endl;
    }
}

int main( int argc, char ** argv )
{	
 
    CmdArgStr	partition_name ('p', "partition", "partition-name", "partition to work in.");
    CmdArgInt	length ('l', "length", "data-length", "size of data for timing test (1024 by default).");

//
// Initialize command line parameters with default values
//
    num = 100;
    iter = 10000;
    delay = 0;
    length = 1024;
    object_name = "ipc_test_object_0_mt";
   
    try {
        IPCCore::init( argc, argv );
    }
    catch( daq::ipc::Exception & ex ) {
	ers::fatal( ex );
	return 1;
    }

//
// Declare command object and its argument-iterator
//       
    CmdLine  cmd(*argv, &num, &partition_name, &synch, &asynch, &check, &iter, &echo, &object_name, &delay, &length, NULL);
    CmdArgvIter  arg_iter(--argc, ++argv);

//
// Parse arguments
//
    cmd.parse(arg_iter);
  
    IPCPartition   p( (const char*)partition_name );
  
    TestIterators(p);
    TestLookupMethod(p);
    TestIsObjectValid(p);
    TestRemoteInvocation(p);
    TestExceptions(p);
    TestCallbacks(p);
    TestCache(p);
    TestPerformance( p, length );    
    TestResolve(p);
    TestTestable(p);
    TestAMI(p);
        
    std::cout << "# of failed methods invocations: " << std::endl
	<< "\tasynchronous - " << a_error_num << std::endl
	<< "\tsynchronous - " << s_error_num << std::endl;
  
    ipc::server_var handle;
    try {
	handle = p.lookup<ipc::server>( (const char*)object_name );
    }
    catch( daq::ipc::InvalidPartition & ex ) {
	ers::error( ex );
	return 1;
    }
    catch( daq::ipc::ObjectNotFound & ex ) {
	ers::error( ex );
	return 1;
    }
    catch( daq::ipc::InvalidObjectName & ex ) {
	ers::error( ex );
	return 1;
    }

    try
    {
    	handle -> shutdown( );
    }
    catch(CORBA::SystemException & ex) 
    {
	std::cerr << "ERROR [ipc_test_client::main] Call to method shutdown failed " << ex._name() << std::endl;
	return 1;
    }
    	
    return 0;
}
