
//
//      test-server.cc
//
//      test application for IPC library
//
//      Sergei Kolos January 2000
//
//      description:
//              Implements test.accessible interface
//        Creates several implementation objects
//        Performs timing for the publish and withdraw operations
/////////////////////////////////////////////////////////////////////////////////
#include <unistd.h>

#include <iostream>
#include <iomanip>

#include <ers/ers.h>

#include <ipc/namedobject.h>
#include <ipc/object.h>
#include <ipc/alarm.h>
#include <ipc/core.h>
#include <ipc/signal.h>

#include <owl/timer.h>
#include <cmdl/cmdargs.h>

#include <test/test.hh>

CmdArgBool echo ('e', "echo", "output results to stdout.");

class IPCTestable : public IPCNamedObject<POA_ipc::servant>
{
  public:
    IPCTestable( IPCPartition & p, const char * name )
      : IPCNamedObject<POA_ipc::servant>( p, name )
    {
	try {
	    publish();
	}
	catch( daq::ipc::InvalidPartition & ex ) {
	    ers::error( ex );
	}
	catch( daq::ipc::InvalidObjectName & ex ) {
	    ers::error( ex );
	}
    }
    
    void test (const char* p) {
    	std::clog << "Test is called with the '" << p << "' parameters" << std::endl;
    }
};

template <class TP = ipc::single_thread>
class IPCTest : public IPCNamedObject<POA_ipc::server,TP>
{
  public:
    IPCTest( IPCPartition & p, const char * name )
      : IPCNamedObject<POA_ipc::server, TP>( p, name )
    {
	num_a = num_s = 0;
	try {
	    IPCNamedObject<POA_ipc::server,TP>::publish();
	}
	catch( daq::ipc::InvalidPartition & ex ) {
	    ers::error( ex );
	}
	catch( daq::ipc::InvalidObjectName & ex ) {
	    ers::error( ex );
	}
    }
	  
    IPCTest( IPCPartition & p, const char * name, const IPCTest<ipc::single_thread> & obj )
      : IPCNamedObject<POA_ipc::server, ipc::single_thread>( p, name, &obj )
    {
	num_a = num_s = 0;
	try {
    	    IPCNamedObject<POA_ipc::server,TP>::publish();
	}
	catch( daq::ipc::InvalidPartition & ex ) {
	    ers::error( ex );
	}
	catch( daq::ipc::InvalidObjectName & ex ) {
	    ers::error( ex );
	}
    }
	  
    ~IPCTest()
    {
	std::cout << "~IPCTest : "
		<< IPCNamedObject<POA_ipc::server,TP>::name() << std::endl;
	if ( echo ) 
	{
            std::cout << "Synchronous method has been called "
        	    << num_s << " times" << std::endl;
            std::cout << "Asynchronous method has been called "
        	    << num_a << " times" << std::endl;
	}
	try {
    	    IPCNamedObject<POA_ipc::server,TP>::withdraw();
	}
	catch( daq::ipc::InvalidPartition & ex ) {
	    ers::warning( ex );
	}
	catch( daq::ipc::InvalidObjectName & ex ) {
	    ers::error( ex );
	}
	catch( daq::ipc::ObjectNotFound & ex ) {
	    ers::error( ex );
	}
    }
  
    virtual char * ping_s ( const char * str )
    {
	active++;
	if ( echo ) 
	    std::cout << IPCNamedObject<POA_ipc::server,TP>::name()
	    << " ::[" << active << "] :: ping_s is called by the "
	    << pthread_self() << " thread" << std::endl;
	num_s++;    
	char * r = new char[ strlen(str) + 1 ];
	strcpy( r, str );
	active--;
	return r;
    }
    
    virtual void    ping_a ( const char * )
    {
	active++;
	if ( echo ) 
	    std::cout << IPCNamedObject<POA_ipc::server,TP>::name()
	    << " ::[" << active << "] :: ping_a is called by the "
	    << pthread_self() << " thread" << std::endl;
	num_a++;
	active--;
	return;
    }
    
    void shutdown( )
    { 
	std::cout << "[IPCTest::shutdown] shutdown method called for the object \""
		<< IPCNamedObject<POA_ipc::server,TP>::name() << "\"" << std::endl;

        daq::ipc::signal::raise();
    }

  private:  
    static int active;
    int    num_a;
    int    num_s;
};

template <class TP>
int IPCTest<TP>::active = -1;

class IPCPerson : public IPCNamedObject<POA_ipc::person, ipc::single_thread>
{
  public:
    IPCPerson( IPCPartition & p, const char * name )
      : IPCNamedObject<POA_ipc::person, ipc::single_thread>( p, name ),
        age_( 0 )
    {
	try {
    	    publish();
	}
	catch( daq::ipc::InvalidPartition & ex ) {
	    ers::error( ex );
	}
	catch( daq::ipc::InvalidObjectName & ex ) {
	    ers::error( ex );
	}
    }

    IPCPerson( IPCPartition & p, const char * name, const IPCPerson & person )
      : IPCNamedObject<POA_ipc::person, ipc::single_thread>( p, name, &person ),
        age_( 0 )
    {
	try {
    	    publish();
	}
	catch( daq::ipc::InvalidPartition & ex ) {
	    ers::error( ex );
	}
	catch( daq::ipc::InvalidObjectName & ex ) {
	    ers::error( ex );
	}    
    }
	  
    ~IPCPerson()
    {
	std::cout << "~IPCPerson : " << name() << std::endl;
	try {
    	    withdraw();
	}
	catch( daq::ipc::InvalidPartition & ex ) {
	    ers::warning( ex );
	}
	catch( daq::ipc::InvalidObjectName & ex ) {
	    ers::error( ex );
	}
	catch( daq::ipc::ObjectNotFound & ex ) {
	    ers::error( ex );
	}
    }
  
    char * Name ( )
    {
	return CORBA::string_dup( name().c_str() );
    }
    
    short Age( )
    {
	return age_++;
    }
    
    void shutdown( )
    { 
	std::cout << "[IPCPerson::shutdown] shutdown method called for the object \""
		<< name() << "\"" << std::endl;

	daq::ipc::signal::raise();
    }
    
  private:  
    short    age_;
};


class IPCEmployee : public POA_ipc::employee, 
		    public IPCPerson
{
  public:
    IPCEmployee( IPCPartition & p, const char * name )
      : IPCPerson( p, name ),
        salary_( 0 ),
	alarm_( 0 )
    {
	try {
    	    publish();
	}
	catch( daq::ipc::InvalidPartition & ex ) {
	    ers::error( ex );
	}
	catch( daq::ipc::InvalidObjectName & ex ) {
	    ers::error( ex );
	}
    }
	  
    IPCEmployee( IPCPartition & p, const char * name, const IPCEmployee & e )
      : IPCPerson( p, name, e ),
        salary_( 0 ),
	alarm_( 0 )
    {
	try {
    	    publish();
	}
	catch( daq::ipc::InvalidPartition & ex ) {
	    ers::error( ex );
	}
	catch( daq::ipc::InvalidObjectName & ex ) {
	    ers::error( ex );
	}
    }
	  
    ~IPCEmployee()
    {
	std::cout << "~IPCEmployee : " << name() << std::endl;
	try {
    	    withdraw();
	}
	catch( daq::ipc::InvalidPartition & ex ) {
	    ers::warning( ex );
	}
	catch( daq::ipc::InvalidObjectName & ex ) {
	    ers::error( ex );
	}
	catch( daq::ipc::ObjectNotFound & ex ) {
	    ers::error( ex );
	}

	if ( alarm_ )
        {
	    delete alarm_;
        }
    }
  
    short Salary ( )
    {
	return salary_;
    }
    
    static bool callback( void * param )
    {
	IPCEmployee * emp = static_cast<IPCEmployee*>( param );
	std::cout << "[IPCEmployee] alarm called - notifying subscriber" << std::endl;
    	try
	{
	    emp->cb_->notify( emp->name().c_str() );
	}
	catch(...)
	{
	    std::cerr << "callback : notification fails" << std::endl;
	    return false;
	}
	return true;
    }
    
    void subscribe ( ipc::callback_ptr cb, short interval )
    {
	std::cout << "[IPCEmployee] subscribe requested" << std::endl;
	cb_ = ipc::callback::_duplicate( cb );
	alarm_ = new IPCAlarm( interval, 0, callback, this );
    }

  private:  
    short		salary_;
    ipc::callback_var	cb_;
    IPCAlarm *		alarm_;
};


//////////////////////////////////////////////////////////////
//
// Test for IPCNamedObject
//
//////////////////////////////////////////////////////////////
void testObjectManager( IPCPartition & p )
{
   IPCTest<> * t1_0 = new IPCTest<>( p, "t1_0" );   

   IPCTest<> * t1_1 = new IPCTest<>( p, "t1_1", *t1_0 );
   
   IPCTest<> * t1_2 = new IPCTest<>( p, "t1_2", *t1_1 );

   IPCPerson * michey = new IPCPerson( p, "Michey" );
   
   IPCPerson * bob = new IPCPerson( p, "Bob", *michey );
  
   IPCEmployee * david = new IPCEmployee( p, "David" );
  
   IPCEmployee * kurt = new IPCEmployee( p, "Kurt", *david );
   
   t1_0->_destroy();
   t1_1->_destroy();
   t1_2->_destroy();
   michey->_destroy();
   bob->_destroy();
   david->_destroy();
   kurt->_destroy();
}

//////////////////////////////////////////
//
// Main function
//
//////////////////////////////////////////

int main ( int argc, char ** argv )
{
   CmdArgStr	partition_name	('p', "partition", "partition-name", "partition to work.");
   CmdArgStr	object_name	('o', "object", "object-name", "object name.");
   CmdArgInt	object_number	('N', "Number","object-number", "number of objects to be created (1 by default).");
 
    try {
        IPCCore::init( argc, argv );
    }
    catch( daq::ipc::Exception & ex ) {
	ers::fatal( ex );
        return 1;
    }
   
       // Declare command object and its argument-iterator
   CmdLine  cmd(*argv, &partition_name, &object_number, &echo, &object_name, NULL);
   CmdArgvIter  arg_iter(--argc, ++argv);
  
    // Initialize command line parameters with default values

   object_number = 1;     
   object_name = "ipc_test_object";
       // Parse arguments

   cmd.parse(arg_iter);

    // Test various IPCPartition constructors

   IPCPartition     partition( (const char*)partition_name );
   IPCPartition     partition1( partition );
   IPCPartition     partition2 = partition;
        
   testObjectManager( partition );
   
   IPCTest<> * t1_0 = new IPCTest<>( partition, "t1_0" );   

   IPCTest<> * t1_1 = new IPCTest<>( partition, "t1_1", *t1_0 );
   
   IPCTest<> * t1_2 = new IPCTest<>( partition, "t1_2", *t1_1 );

   IPCPerson * michey = new IPCPerson( partition, "Michey" );
   
   IPCPerson * bob = new IPCPerson( partition, "Bob", *michey );
  
   IPCEmployee * david = new IPCEmployee( partition, "David" );
  
   IPCEmployee * kurt = new IPCEmployee( partition, "Kurt", *david );
   
   IPCTestable * test1 = new IPCTestable( partition, "testable object 1");

   IPCTestable * test2 = new IPCTestable( partition, "testable object 2");
   
   IPCTest<ipc::multi_thread> ** mt_objectList = new IPCTest<ipc::multi_thread>*[object_number];  
   IPCTest<ipc::single_thread> ** st_objectList = new IPCTest<ipc::single_thread>*[object_number];  
//
// Record starting time
//
   OWLTimer    time;  
   time.start();
     
   {
   	IPCTest<> * bad_object = new IPCTest<>( partition, "" );
        bad_object -> _destroy();
   }
   
   mt_objectList[0] = new IPCTest<ipc::multi_thread>( partition, "ipc_test_object_0_mt" );
   st_objectList[0] = new IPCTest<ipc::single_thread>( partition, "ipc_test_object_0_st" );
   
   int i;
   char oname_mt[32];
   char oname_st[32];
   for ( i = 1; i < object_number ; i++ )
   {
	sprintf(oname_mt,"%s_%d_mt",(const char*)object_name, i);
	sprintf(oname_st,"%s_%d_st",(const char*)object_name, i);
	mt_objectList[i] = new IPCTest<ipc::multi_thread>( partition, oname_mt );
	st_objectList[i] = new IPCTest<ipc::single_thread>( partition, oname_st, *(st_objectList[0]) );
   }
  
   time.stop();
   if ( echo )
   {
	std::cout << object_number << " objects have been created." << std::endl;
	std::cout << "It takes (in seconds): " << std::endl
		  << "\t" << time.userTime()/object_number << "(user time)" << std::endl
		  << "\t" << time.systemTime()/object_number << "(system time)" << std::endl
		  << "\t" << time.totalTime()/object_number << "(total time) " << std::endl;
   }

//
// Test alarms
//
   if ( echo )
   {
	std::cout << "testing alarms ... " << std::endl;
   }
       
    //
    // Block the main thread
    //
   std::cout << "ipc_test_server has been started." << std::endl;
   
   daq::ipc::signal::wait_for();
  
   t1_0->_destroy();
   t1_1->_destroy();
   t1_2->_destroy();
   michey->_destroy();
   bob->_destroy();
   david->_destroy();
   kurt->_destroy();
   test1->_destroy();
   test2->_destroy();
     
//
// Test object destruction and make timing of object withdrawal
//
   if ( echo )
   {
	std::cout << " Server has been stopped." << std::endl;
	std::cout << " Destroying objects ..." ;
   }
  
   time.reset();
   time.start();
  
   for ( i = 0; i < object_number ; i++ )
   {
	mt_objectList[i]->_destroy();
	st_objectList[i]->_destroy();
   }
  
   time.stop();
   
   if ( echo )
   {
	std::cout << "done." << std::endl;
	std::cout << object_number << " objects have been destroyed." << std::endl;
	std::cout << "It takes (in seconds): " << std::endl
	     << "\t" << time.userTime()/object_number << "(user time)" << std::endl
	     << "\t" << time.systemTime()/object_number << "(system time)" << std::endl
	     << "\t" << time.totalTime()/object_number << "(total time) " << std::endl;
   }
   
   delete[] mt_objectList;
   
   if ( echo )
   {
	std::cout << "Test successfully completed." << std::endl;
   }
      
   return 0;
}
