#include <ers/ers.h>

#include <cmdl/cmdargs.h>

#include <ipc/threadpool.h>

struct job
{
    job( int id, int delay )
      : id_( id ),
	delay_( delay )
    { ; }

    void operator()()
    {
	ERS_DEBUG( 1, "Job " << id_ << " has been started and will last for "
	    << delay_ << " microseconds" );

	::usleep( delay_ );        
        
	ERS_DEBUG( 1, "Job " << id_ << " has been finished after "
	    << delay_ << " microseconds" );
    }

  private:
    int	id_;
    int	delay_;
};

int main( int argc, char ** argv )
{
    CmdArgInt	short_jobs( 's', "short-jobs", "number", "number of short-living jobs (1000).");
    CmdArgInt	long_jobs( 'l', "long-jobs", "number", "number of long-living jobs (10).");
    CmdArgInt	delay( 'd', "delay", "microseconds", "delay for a short-living job (1000).");
    CmdArgInt	Delay( 'D', "Delay", "microseconds", "delay for a long-living job (1000000).");
    CmdArgInt	threads( 't', "threads", "number", "number of threads in the pool (10).");
    
    CmdLine	cmd( *argv, &short_jobs, &long_jobs, &delay, &threads, &Delay, NULL);
    CmdArgvIter	arg_iter(--argc, ++argv);

    short_jobs = 1000;
    long_jobs = 10;
    delay = 1000;
    threads = 10;
    Delay = 1000000;
    
    cmd.parse(arg_iter);

    IPCThreadPool threadpool( threads );
    
    int k = short_jobs/long_jobs;
    ERS_LOG( "Starting jobs" );
    for ( int i = 0; i < long_jobs; i++ )
    {
	for ( int j = 0; j < short_jobs/long_jobs; j++ )
	{
	    threadpool.addJob( job( i*k + j, delay )  );
	}
        threadpool.addJob( job( ( i + 1 )*k, Delay )  );
    }
    
    threadpool.waitForCompletion();
    ERS_LOG( "All jobs have been finished" );
    
    return 0;
}
